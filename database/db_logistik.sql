-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2014 at 03:58 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_logistik`
--
CREATE DATABASE IF NOT EXISTS `db_logistik` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_logistik`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_barang` (
  `kd_barang` varchar(5) NOT NULL,
  `kd_jns_barang` varchar(8) NOT NULL,
  `nm_barang` varchar(50) NOT NULL,
  `harga_barang` int(20) NOT NULL,
  `stok` int(10) NOT NULL,
  PRIMARY KEY (`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`kd_barang`, `kd_jns_barang`, `nm_barang`, `harga_barang`, `stok`) VALUES
('B-001', 'J-001', 'Sapu', 15000, 3),
('B-002', 'J-002', 'Kasur Double', 2500000, 7),
('B-003', 'J-001', 'Kain Pel', 10000, 7),
('B-004', 'J-003', 'Spidol Marker', 12000, 1),
('B-005', 'J-004', 'Amplop Besar ', 3000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jns_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_jns_barang` (
  `kd_jns_barang` varchar(5) NOT NULL DEFAULT '',
  `jns_barang` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`kd_jns_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jns_barang`
--

INSERT INTO `tbl_jns_barang` (`kd_jns_barang`, `jns_barang`) VALUES
('J-001', 'Alat Kebersihan'),
('J-002', 'Perlengkapan Kamar'),
('J-003', 'Alat Tulis'),
('J-004', 'Percetakan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE IF NOT EXISTS `tbl_member` (
  `kd_member` varchar(5) NOT NULL,
  `nm_member` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` enum('admin','staf','gudang') NOT NULL DEFAULT 'admin',
  `kd_unit` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`kd_member`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`kd_member`, `nm_member`, `username`, `password`, `level`, `kd_unit`) VALUES
('M-001', 'Gilang Sonar', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'U-002'),
('M-002', 'Gareng', 'gareng', '97e081dc0f2ed4c055bfee8b8b042b08', 'staf', 'U-001'),
('M-003', 'John Doe', 'john', '527bd5b5d689e2c32ae974c6229ff785', 'gudang', 'U-005'),
('M-004', 'Joker Man', 'joker', '9facbf452def2d7efc5b5c48cdb837fa', 'staf', 'U-004');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengadaan_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_pengadaan_detail` (
  `kd_pengadaan` varchar(8) NOT NULL,
  `kd_barang` varchar(8) NOT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengadaan_header`
--

CREATE TABLE IF NOT EXISTS `tbl_pengadaan_header` (
  `kd_pengadaan` varchar(8) NOT NULL,
  `kd_supplier` varchar(5) NOT NULL,
  `total_harga` varchar(20) DEFAULT NULL,
  `kd_member` varchar(5) NOT NULL,
  `tgl_pengadaan` date NOT NULL,
  PRIMARY KEY (`kd_pengadaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengeluaran_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_pengeluaran_detail` (
  `kd_pengeluaran` varchar(8) NOT NULL,
  `kd_barang` varchar(8) NOT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengeluaran_header`
--

CREATE TABLE IF NOT EXISTS `tbl_pengeluaran_header` (
  `kd_pengeluaran` varchar(8) NOT NULL,
  `kd_unit` varchar(5) NOT NULL,
  `total_harga` varchar(20) DEFAULT NULL,
  `kd_member` varchar(5) NOT NULL,
  `tgl_pengeluaran` date NOT NULL,
  `kd_permintaan` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`kd_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permintaan_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_permintaan_detail` (
  `kd_permintaan` varchar(8) NOT NULL,
  `kd_barang` varchar(8) NOT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permintaan_header`
--

CREATE TABLE IF NOT EXISTS `tbl_permintaan_header` (
  `kd_permintaan` varchar(8) NOT NULL,
  `kd_unit` varchar(5) NOT NULL,
  `stts` enum('ok','pending') DEFAULT 'pending',
  `kd_member` varchar(5) NOT NULL,
  `tgl_permintaan` date NOT NULL,
  `tgl_dibutuhkan` date DEFAULT NULL,
  PRIMARY KEY (`kd_permintaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE IF NOT EXISTS `tbl_supplier` (
  `kd_supplier` varchar(5) NOT NULL,
  `nm_supplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `kontak` varchar(12) NOT NULL,
  `email` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`kd_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`kd_supplier`, `nm_supplier`, `alamat`, `kontak`, `email`) VALUES
('S-001', 'CV. Sejahtera', 'Bantul, Yogyakarta', '08123456789', 'sejahtera@mail.com'),
('S-002', 'UD. Toko Bagus', 'Jl. Solo, Yogyakarta', '08123456789', 'tokobagus@mail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unit`
--

CREATE TABLE IF NOT EXISTS `tbl_unit` (
  `kd_unit` varchar(5) NOT NULL,
  `nm_unit` varchar(50) NOT NULL,
  PRIMARY KEY (`kd_unit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_unit`
--

INSERT INTO `tbl_unit` (`kd_unit`, `nm_unit`) VALUES
('U-001', 'HRD / Personalia'),
('U-002', 'SI'),
('U-003', 'Executive Lounge'),
('U-004', 'Humas'),
('U-005', 'Gudang');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
