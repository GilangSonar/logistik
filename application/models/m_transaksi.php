<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/20/13
 * Time: 3:07 AM
 */
class M_transaksi extends CI_Model{
    function __construct(){
        parent::__construct();
    }

//    CREATE KODE
    function kodePermintaan(){
        $q = $this->db->query("select MAX(RIGHT(kd_permintaan,5)) as code_max from tbl_permintaan_header");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%05s", $tmp);
            }
        }else{
            $code = "00001";
        }
        return "PM-".$code;
    }
    function kodePengadaan(){
        $q = $this->db->query("select MAX(RIGHT(kd_pengadaan,5)) as code_max from tbl_pengadaan_header");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%05s", $tmp);
            }
        }else{
            $code = "00001";
        }
        return "PG-".$code;
    }
    function kodePengeluaran(){
        $q = $this->db->query("select MAX(RIGHT(kd_pengeluaran,5)) as code_max from tbl_pengeluaran_header");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%05s", $tmp);
            }
        }else{
            $code = "00001";
        }
        return "PL-".$code;
    }

//    MANAJEMEN STOK
    public function kurangiStok($kd_barang,$kurangi){
        $q = $this->db->query("select stok from tbl_barang where kd_barang='".$kd_barang."'");
        $stok = "";
        foreach($q->result() as $d){
            $stok = $d->stok - $kurangi;
        }
        return $stok;
    }

    public function tambahStok($kd_barang,$tambahi){
        $q = $this->db->query("select stok from tbl_barang where kd_barang='".$kd_barang."'");
        $stok = "";
        foreach($q->result() as $d){
            $stok = $d->stok + $tambahi;
        }
        return $stok;
    }

    public function kembalikanStok($kd_barang){
        $q = $this->db->query("select stok from tbl_barang where kd_barang='".$kd_barang."'");
        $stok = "";
        foreach($q->result() as $d){
            $stok = $d->stok;
        }
        return $stok;
    }

//   ============ // GET DATA // ===========

//  ( Permintaan )
    function getPermintaan(){
        return $this->db->query("
            SELECT a.kd_permintaan,a.tgl_permintaan, a.tgl_dibutuhkan,a.stts,b.nm_unit,
			(select count(kd_permintaan) as jum from tbl_permintaan_detail where kd_permintaan=a.kd_permintaan) as jumlah 
			from tbl_permintaan_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
        ")->result();
    }
    function getPermintaanHeaderById($id){
        return $this->db->query("
            SELECT a.*, b.nm_unit
			from tbl_permintaan_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
			where a.kd_permintaan='$id'
        ")->result();
    }
    function getPermintaanDetailById($id){
        return $this->db->query("
            SELECT a.*,b.nm_barang,b.harga_barang
			from tbl_permintaan_detail a left join tbl_barang b
			on a.kd_barang=b.kd_barang
			where a.kd_permintaan='$id'
        ")->result();
    }
    function getPermintaanByUnit($id){
        return $this->db->query("
            SELECT a.kd_permintaan,a.tgl_permintaan,a.tgl_dibutuhkan,a.stts, b.nm_unit,
			(select count(kd_permintaan) as jum from tbl_permintaan_detail where kd_permintaan=a.kd_permintaan) as jumlah
			from tbl_permintaan_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
			where a.kd_unit='$id'
        ")->result();
    }
    function getPermintaanPending(){
        return $this->db->query("
            SELECT a.kd_permintaan,a.tgl_permintaan,a.tgl_dibutuhkan,a.stts, b.nm_unit,
			(select count(kd_permintaan) as jum from tbl_permintaan_detail where kd_permintaan=a.kd_permintaan) as jumlah
			from tbl_permintaan_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
			where a.stts='pending'
        ")->result();
    }

//  ( Pengadaan )
    function getPengadaan(){
        return $this->db->query("
            SELECT a.kd_pengadaan,a.tgl_pengadaan, b.nm_supplier,
			(select count(kd_pengadaan) as jum from tbl_pengadaan_detail where kd_pengadaan=a.kd_pengadaan) as jumlah
			from tbl_pengadaan_header a left join tbl_supplier b
			on a.kd_supplier=b.kd_supplier
        ")->result();
    }
    function getPengadaanHeaderById($id){
        return $this->db->query("
            SELECT a.*, b.nm_supplier
			from tbl_pengadaan_header a left join tbl_supplier b
			on a.kd_supplier=b.kd_supplier
			where a.kd_pengadaan='$id'
        ")->result();
    }
    function getPengadaanDetailById($id){
        return $this->db->query("
            SELECT a.*,b.nm_barang,b.harga_barang
			from tbl_pengadaan_detail a left join tbl_barang b
			on a.kd_barang=b.kd_barang
			where a.kd_pengadaan='$id'
        ")->result();
    }


    // (Pengeluaran)
    function getPengeluaran(){
        return $this->db->query("
            SELECT a.kd_pengeluaran,a.tgl_pengeluaran, b.nm_unit,
			(select count(kd_pengeluaran) as jum from tbl_pengeluaran_detail where kd_pengeluaran=a.kd_pengeluaran) as jumlah 
			from tbl_pengeluaran_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
        ")->result();
    }
    function getPengeluaranHeaderById($id){
        return $this->db->query("
            SELECT a.*, b.nm_unit,d.tgl_permintaan,d.tgl_dibutuhkan
			from tbl_pengeluaran_header a
			left join tbl_unit b on a.kd_unit=b.kd_unit
			left join tbl_permintaan_header d on a.kd_permintaan=d.kd_permintaan
			where a.kd_pengeluaran='$id'
        ")->result();
    }
    function getPengeluaranDetailById($id){
        return $this->db->query("
            SELECT a.*,b.nm_barang,b.harga_barang
			from tbl_pengeluaran_detail a left join tbl_barang b
			on a.kd_barang=b.kd_barang
			where a.kd_pengeluaran='$id'
        ")->result();
    }

//    ============ // INSERT DATA // ===========

//  ( Permintaan )
    function insertPermintaanHeader($data){
        $query = $this->db->insert('tbl_permintaan_header',$data);
        return $query;
    }
    function insertPermintaanDetail($data){
        $query = $this->db->insert('tbl_permintaan_detail',$data);
        return $query;
    }
//  ( Pengeluaran )
    function insertPengeluaranHeader($data){
        $query = $this->db->insert('tbl_pengeluaran_header',$data);
        return $query;
    }
    function insertPengeluaranDetail($data){
        $query = $this->db->insert('tbl_pengeluaran_detail',$data);
        return $query;
    }
//  ( Pengadaan )
    function insertPengadaanHeader($data){
        $query = $this->db->insert('tbl_pengadaan_header',$data);
        return $query;
    }
    function insertPengadaanDetail($data){
        $query = $this->db->insert('tbl_pengadaan_detail',$data);
        return $query;
    }


//   ============ // UPDATE DATA // ===========

//  ( Permintaan )
    function updatePermintaanHeader($id,$data){
        $this->db->where('kd_permintaan',$id);
        $update = $this->db->update('tbl_permintaan_header',$data);
        return $update;
    }
//  ( Pengeluaran )
    function updatePengeluaranHeader($id,$data){
        $this->db->where('kd_pengeluaran',$id);
        $update = $this->db->update('tbl_pengeluaran_header',$data);
        return $update;
    }
//  ( Pengadaan )
    function updatePengadaanHeader($id,$data){
        $this->db->where('kd_pengadaan',$id);
        $update = $this->db->update('tbl_pengadaan_header',$data);
        return $update;
    }
//   ============ // DELETE DATA // ===========

    //  ( Permintaan )
    function deletePermintaan($id){
        $table=array('tbl_permintaan_header','tbl_permintaan_detail');
        $this->db->where('kd_permintaan',$id);
        $del = $this->db->delete($table);
        return $del;
    }
    function deletePermintaanDetail($id){
        $this->db->where('kd_permintaan',$id);
        $del = $this->db->delete('tbl_permintaan_detail');
        return $del;
    }
    //  ( Pengadaan )
    function deletePengadaan($id){
        $table=array('tbl_pengadaan_header','tbl_pengadaan_detail');
        $this->db->where('kd_pengadaan',$id);
        $del = $this->db->delete($table);
        return $del;
    }
    function deletePengadaanDetail($id){
        $this->db->where('kd_pengadaan',$id);
        $del = $this->db->delete('tbl_pengadaan_detail');
        return $del;
    }
    //  ( Pengeluaran )
    function deletePengeluaran($id){
        $table=array('tbl_pengeluaran_header','tbl_pengeluaran_detail');
        $this->db->where('kd_pengeluaran',$id);
        $del = $this->db->delete($table);
        return $del;
    }
}