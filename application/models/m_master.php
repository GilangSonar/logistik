<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 9:08 PM
 */
class M_master extends CI_Model{
    function __construct(){
        parent::__construct();
    }

//    CREATE KODE
    function kodeJenisBarang(){
        $q = $this->db->query("select MAX(RIGHT(kd_jns_barang,3)) as code_max from tbl_jns_barang");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "01";
        }
        return "J-".$code;
    }
    function kodeBarang(){
        $q = $this->db->query("select MAX(RIGHT(kd_barang,3)) as code_max from tbl_barang");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "B-".$code;
    }
    function kodeUnit(){
        $q = $this->db->query("select MAX(RIGHT(kd_unit,3)) as code_max from tbl_unit");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "U-".$code;
    }
    function kodeSupplier(){
        $q = $this->db->query("select MAX(RIGHT(kd_supplier,3)) as code_max from tbl_supplier");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "S-".$code;
    }
    function kodeMember(){
        $q = $this->db->query("select MAX(RIGHT(kd_member,3)) as code_max from tbl_member");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "M-".$code;
    }

//    GET ALL DATA
    function getAllJenisBarang(){
        return $this->db->query("select * from tbl_jns_barang")->result();
    }
    function getAllBarang(){
        return $this->db->query("select * from tbl_barang a left join tbl_jns_barang b on a.kd_jns_barang=b.kd_jns_barang")->result();
    }
    function getAllUnit(){
        return $this->db->query("select * from tbl_unit")->result();
    }
    function getAllSupplier(){
        return $this->db->query("select * from tbl_supplier")->result();
    }
    function getAllMember(){
        return $this->db->query("select * from tbl_member a left join tbl_unit b on a.kd_unit=b.kd_unit")->result();
    }


//    GET DATA BY ID
    function getBarangById($id){
        return $this->db->query("select * from tbl_barang a left join tbl_jns_barang b on a.kd_jns_barang=b.kd_jns_barang
        where a.kd_barang='$id'")->result();
    }
    function getBarangPengeluaran($id){
        return $this->db->query("select * from tbl_barang a left join tbl_jns_barang b on a.kd_jns_barang=b.kd_jns_barang
        where a.kd_jns_barang='$id' AND a.stok > 0 ")->result();
    }
    function getBarangPengadaan($id){
        return $this->db->query("select * from tbl_barang a left join tbl_jns_barang b on a.kd_jns_barang=b.kd_jns_barang
        where a.kd_jns_barang='$id' AND a.stok < 5 ")->result();
    }
    function getMemberById($id){
        return $this->db->query("select * from tbl_member a left join tbl_unit b on a.kd_unit=b.kd_unit where a.kd_member='$id'")->result();
    }


//    INSERT DATA
    function insertJenisBarang($data){
        $query = $this->db->insert('tbl_jns_barang',$data);
        return $query;
    }
    function insertBarang($data){
        $query = $this->db->insert('tbl_barang',$data);
        return $query;
    }
    function insertUnit($data){
        $query = $this->db->insert('tbl_unit',$data);
        return $query;
    }
    function insertSupplier($data){
        $query = $this->db->insert('tbl_supplier',$data);
        return $query;
    }
    function insertMember($data){
        $query = $this->db->insert('tbl_member',$data);
        return $query;
    }

//    UPDATE DATA
    function updateJenisBarang($id,$data){
        $this->db->where('kd_jns_barang',$id);
        $update = $this->db->update('tbl_jns_barang',$data);
        return $update;
    }
    function updateBarang($id,$data){
        $this->db->where('kd_barang',$id);
        $update = $this->db->update('tbl_barang',$data);
        return $update;
    }
    function updateUnit($id,$data){
        $this->db->where('kd_unit',$id);
        $update = $this->db->update('tbl_unit',$data);
        return $update;
    }
    function updateSupplier($id,$data){
        $this->db->where('kd_supplier',$id);
        $update = $this->db->update('tbl_supplier',$data);
        return $update;
    }
    function updateMember($id,$data){
        $this->db->where('kd_member',$id);
        $update = $this->db->update('tbl_member',$data);
        return $update;
    }

//    DELETE DATA
    function deleteJenisBarang($id){
        $this->db->where_in('kd_jns_barang',$id);
        $this->db->delete('tbl_jns_barang');
        return $this->db->affected_rows() > 0;
    }
    function deleteBarang($id){
        $this->db->where_in('kd_barang',$id);
        $this->db->delete('tbl_barang');
        return $this->db->affected_rows() > 0;
    }
    function deleteUnit($id){
        $this->db->where_in('kd_unit',$id);
        $this->db->delete('tbl_unit');
        return $this->db->affected_rows() > 0;
    }
    function deleteSupplier($id){
        $this->db->where_in('kd_supplier',$id);
        $this->db->delete('tbl_supplier');
        return $this->db->affected_rows() > 0;
    }
    function deleteMember($id){
        $this->db->where_in('kd_member',$id);
        $this->db->delete('tbl_member');
        return $this->db->affected_rows() > 0;
    }

//    OTHER QUERY
    function getNotifStokBarang(){
        return $this->db->query("select kd_barang,nm_barang,stok from tbl_barang where stok < 5 ");
    }
}