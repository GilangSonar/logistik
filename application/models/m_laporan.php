<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/28/13
 * Time: 2:53 AM
 */
class M_laporan extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    //    PENCARIAN DATA PERMINTAAN
    function getLapPermintaan($tgl_awal,$tgl_akhir){
        return $this->db->query("
            SELECT a.kd_permintaan,a.tgl_permintaan,a.tgl_dibutuhkan,a.stts, b.nm_unit,
			(select count(kd_permintaan) as jum from tbl_permintaan_detail where kd_permintaan=a.kd_permintaan) as jumlah
			from tbl_permintaan_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
			where a.tgl_permintaan between '$tgl_awal' and '$tgl_akhir'
        ")->result();
    }

    //    PENCARIAN DATA PENGELUARAN
    function getLapPengeluaran($tgl_awal,$tgl_akhir){
        return $this->db->query("
            SELECT a.kd_pengeluaran,a.tgl_pengeluaran,a.kd_permintaan, b.nm_unit,
			(select count(kd_pengeluaran) as jum from tbl_pengeluaran_detail where kd_pengeluaran=a.kd_pengeluaran) as jumlah
			from tbl_pengeluaran_header a left join tbl_unit b
			on a.kd_unit=b.kd_unit
			where a.tgl_pengeluaran between '$tgl_awal' and '$tgl_akhir'
        ")->result();
    }

    //    PENCARIAN DATA PENGADAAN
    function getLapPengadaan($tgl_awal,$tgl_akhir){
        return $this->db->query("
            SELECT a.kd_pengadaan,a.tgl_pengadaan, b.nm_supplier,
			(select count(kd_pengadaan) as jum from tbl_pengadaan_detail where kd_pengadaan=a.kd_pengadaan) as jumlah
			from tbl_pengadaan_header a left join tbl_supplier b
			on a.kd_supplier=b.kd_supplier
			where a.tgl_pengadaan between '$tgl_awal' and '$tgl_akhir'
        ")->result();
    }

}