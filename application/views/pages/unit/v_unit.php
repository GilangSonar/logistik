<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-cogs"></i>
                            <span>Unit</span>
                        </h1>
                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Master Data
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Unit</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                            <form action="<?php echo site_url('unit/delete_unit')?>" method="post">
                                <div class="box bordered-box fb-border" style="margin-bottom:0;">
                                    <div class='box-header'>
                                        <div class='title'>
                                            List Unit
                                        </div>
                                        <div class='actions'>
                                            <a class="btn btn-primary" href="#modal-add" data-toggle="modal" role="button">
                                                <i class="icon-plus-sign"></i> Tambah Data
                                            </a>
                                            <button class="btn btn-primary" type="submit" onclick="return confirm('Anda yankin akan menghapus data ini ?');">
                                                <i class="icon-trash"></i> Delete Checked Data
                                            </button>
                                        </div>
                                    </div>

                                    <div class="box-content box-no-padding">
                                        <div class="responsive-table">
                                            <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                                <thead>
                                                <tr>
                                                    <th class="only-checkbox text-center">
                                                        <input class="check-all" type="checkbox">
                                                    </th>
                                                    <th>No</th>
                                                    <th>Kode</th>
                                                    <th>Unit</th>
                                                    <th class="text-center">
                                                        <i class="icon-th-large"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php $no=1; if(isset($dt_unit)){foreach ($dt_unit as $row){ ?>
                                                    <tr>
                                                        <td class="only-checkbox text-center">
                                                            <input type="checkbox" name="cek[]" value="<?php echo $row->kd_unit;?>">
                                                        </td>
                                                        <td><?php echo $no++ ?></td>
                                                        <td><?php echo $row->kd_unit ?></td>
                                                        <td><?php echo $row->nm_unit ?></td>
                                                        <td class="col-xs-1">
                                                            <div class="text-center">
                                                                <a class="btn btn-inverse btn-xs" href="#modal-edit<?php echo $row->kd_unit?>" data-toggle="modal" role="btn">
                                                                    <i class="icon-edit"></i> Edit
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </section>

</div>

<!--MODAL ADD DATA-->
<div class="modal fade" id="modal-add" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
            </div>
            <div class="modal-body">

                <form class="form" style="margin-bottom: 0;" method="post" action="<?php echo site_url('unit/input_unit')?>">
                    <div class="form-group">
                        <label>Kode Unit</label>
                        <input name="kd_unit" class="form-control" type="text" value="<?php echo $kd_unit?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Nama Unit</label>
                        <input name="nm_unit" class="form-control" type="text" required="">
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-inverse" type="submit">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<!--MODAL EDIT DATA-->

<?php if(isset($dt_unit)){foreach ($dt_unit as $row){ ?>

    <div class="modal fade" id="modal-edit<?php echo $row->kd_unit?>" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
                </div>
                <div class="modal-body">

                    <form class="form" style="margin-bottom: 0;" method="post" action="<?php echo site_url('unit/edit_unit')?>">
                        <div class="form-group">
                            <label>Kode Unit</label>
                            <input name="kd_unit" class="form-control" type="text" value="<?php echo $row->kd_unit?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Nama Unit</label>
                            <input name="nm_unit" class="form-control" type="text" value="<?php echo $row->nm_unit?>" required="">
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                            <button class="btn btn-inverse" type="submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

<?php } } ?>