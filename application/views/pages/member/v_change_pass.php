<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-edit"></i>
                            <span>Change Your Password</span>
                        </h1>
                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">
                                    <i class="icon-key"></i> Change Your Password
                                </li>

                            </ul>
                        </div>
                    </div>

                    <!--// END HEADER //-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-header fb-background">
                                    <div class="title">
                                        <div class="icon-edit"></div>
                                        Form Change Password
                                    </div>
                                </div>
                                <div class="box-content">
                                    <?php if(isset($dt_member_id)){foreach ($dt_member_id as $row ) { ?>

                                    <form class="form form-horizontal val-form-change" style="margin-bottom: 0;" method="post" action="<?php echo site_url('member/change_password/'.$row->kd_member)?>">
                                        <div class="group-header group-header-first">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <div class="text-center">
                                                        <h3>Data Member</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Kode</label>
                                            <div class="col-md-5">
                                                <input class="form-control" type="text" value="<?php echo $row->kd_member?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Nama Lengkap</label>
                                            <div class="col-md-5">
                                                <input class="form-control" type="text" value="<?php echo $row->nm_member?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Unit / Bagian </label>
                                            <div class="col-md-5">
                                                <input class="form-control" type="text" value="<?php echo $row->nm_unit?>" readonly>
                                            </div>
                                        </div>

                                        <div class="group-header ">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <div class="text-center">
                                                        <h3>Data Login</h3>
                                                        <small class="text-fb">Silahkan ubah data login sesuai keinginan anda. Pastikan data login anda tidak diketahui oleh pihak lain</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Username </label>
                                            <div class="col-md-5">
                                                <input class="form-control" type="text" name="username" value="<?php echo $row->username?>" maxlength="25">
                                                <small><em>Maksimal 25 Karakter</em></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Password Baru</label>
                                            <div class="col-md-5">
                                                <input id="password" class="form-control" type="password" name="password" required="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="inputPassword4">Konfirmasi Password</label>
                                            <div class="col-md-5">
                                                <input id="confpass" class="form-control" type="password" name="confpass" required="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="inputPassword4">Password Lama</label>
                                            <div class="col-md-5">
                                                <input id="oldpass" class="form-control" type="password" name="oldpass" required="">
                                            </div>
                                        </div>

                                        <div class="form-actions form-actions-padding-sm">
                                            <div class="row">
                                                <div class="col-md-10 col-md-offset-2">
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="icon-save"></i>
                                                        Save
                                                    </button>
                                                    <button class="btn" type="submit">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?php } } ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- // END CONTENT // -->

                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

    //    RULES VALIDATION PERMINTAAN
    $(document).ready(function(){
        $(".val-form-change").validate({
            rules: {
                password: {
                    required:true,
                    minlength: 3
                },
                confpass:{
                    equalTo:"#password",
                    required: true
                }
            }
        });
    })
</script>