<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-edit"></i>
                            <span>Tambah Permintaan</span>
                        </h1>
                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Transaksi
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('permintaan')?>">
                                        <i class="icon-edit"></i> Permintaan
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Tambah Permintaan</li>
                            </ul>
                        </div>
                    </div>

                    <!--// END HEADER //-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box bordered-box fb-border">
                                <div class="box-header fb-background">
                                    <div class="title">
                                        Form Edit Permintaan Barang
                                    </div>
                                </div>
                                <?php if(isset($dt_permintaan_header_id)) { foreach($dt_permintaan_header_id as $row) { ?>
                                <div class="box-content">
                                    <form class="form" method="post" action="<?php echo site_url('permintaan/update_permintaan')?>">

                                        <div class="box-toolbox box-toolbox-top">
                                            <div class="row">
                                                <br/>
                                                <div class="col-sm-6">
                                                    <p>
                                                        <strong>Kode Permintaan Barang:</strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="kd_permintaan"
                                                           value="<?php echo $row->kd_permintaan?>" readonly>
                                                </div>

                                                <div class="col-sm-6">
                                                    <p>
                                                        <strong> Unit Peminta Barang : </strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="kd_unit"
                                                           value="<?php echo $row->kd_unit?> - <?php echo $row->nm_unit?>" readonly>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="box-header">
                                            <div class="title">
                                                Isi Daftar Barang Yang Diminta
                                            </div>
                                            <div class="actions">
                                                <a class="btn btn-primary" href="#modal-add" data-toggle="modal">
                                                    <i class="icon-reorder"></i> Pilih Barang
                                                </a>
                                            </div>
                                        </div>
                                        <div class="responsive-table">
                                            <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                                <thead>
                                                <tr>
                                                    <th>Kode</th>
                                                    <th>Nama Barang</th>
                                                    <th>Permintaan</th>
                                                    <th class="text-center">
                                                        <i class="icon-th-large"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <?php $i=1;?>
                                                <?php foreach($this->cart->contents() as $items): ?>
                                                    <?php echo form_hidden('rowid[]', $items['rowid']); ?>
                                                    <tbody>
                                                    <tr>
                                                        <td><?php echo $items['id']; ?></td>
                                                        <td><?php echo $items['name']; ?></td>
                                                        <td><?php echo $items['qty']; ?></td>
                                                        <td class="col-xs-1">
                                                            <div class="text-center">
                                                                <a href="#modal-edit<?php echo $items['id']?>" data-toggle="modal"
                                                                   class="btn has-tooltip btn-primary" data-placement="top" title="Edit Quantity">
                                                                    <i class="icon-edit"></i>
                                                                </a>
                                                                <a href="#" id="<?php echo 'hapus/'.$items['rowid'];?>"
                                                                   class="btn has-tooltip btn-primary delbutton" data-placement="top" title="Delete">
                                                                    <i class="icon-trash"></i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                    <?php $i++;?>
                                                <?php endforeach; ?>
                                            </table>
                                        </div>

                                        <div class="box-toolbox box-toolbox-bottom">
                                            <div class="row">
                                                <br/>

                                                <div class="col-sm-4">
                                                    <p>
                                                        <strong>Tanggal Dibutuhkan:</strong>
                                                    </p>
                                                    <div>
                                                        <div class="datepicker-input input-group" id="datepicker">
                                                            <input class="form-control" name="tgl_dibutuhkan" placeholder="Pilih Tanggal" type="text" value="<?php echo date("d M Y",strtotime($row->tgl_dibutuhkan))?>" required="">
                                                            <span class="input-group-addon">
                                                                <span data-date-icon="icon-calendar" data-time-icon="icon-time"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <input type="hidden" name="tgl_permintaan" value="<?php echo date('d M Y')?>">

                                        <hr class="hr-normal"/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="icon-save"></i> Update
                                                </button>
                                                <a href="<?php echo site_url('permintaan')?>" class="btn btn-default">
                                                    <i class="icon-remove-circle"></i> Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php } } ?>
                            </div>
                        </div>
                    </div>

                    <!-- // END CONTENT // -->

                </div>
            </div>
        </div>
    </section>
</div>


<!--MODAL EDIT DATA-->
<?php $i=1;?>
<?php foreach($this->cart->contents() as $items): ?>
    <?php echo form_hidden('rowid[]', $items['rowid']); ?>
    <div class="modal" id="modal-edit<?php echo $items['id']?>" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                    <h4 class="modal-title" id="myModalLabel">Pilih Barang</h4>
                </div>
                <div class="modal-body">

                    <form class="form" style="margin-bottom: 0;" method="post" action="<?php echo site_url('permintaan/edit_barang_pesan')?>">
                        <div class="form-group">
                            <label>Kode Barang</label>
                            <input name="kd_barang" class="form-control" type="text" value="<?php echo $items['id']?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input name="nm_barang" class="form-control" type="text" value="<?php echo $items['name']?>" readonly>
                        </div>

                        <div class="form-group">
                            <label>Jumlah Barang Yang Diminta</label>
                            <input name="qty" class="form-control" type="text" required="">
                        </div>

                        <input name="harga_barang" type="hidden" value="<?php echo $items['price']?>">

                        <div class="modal-footer">
                            <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                            <button class="btn btn-inverse" type="submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <?php $i++;?>
<?php endforeach; ?>


<!--MODAL ADD DATA-->
<div class="modal" id="modal-add" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">Pilih Barang</h4>
            </div>
            <div class="modal-body">

                <form class="val-form-permintaan" style="margin-bottom: 0;" method="post" action="<?php echo site_url('permintaan/edit_barang_pesan')?>">
                    <div class="form-group">
                        <label>Jenis Barang</label>
                        <select class="form-control" id="selectJenis" name="kd_jns_barang" required="">
                            <option value=""></option>
                            <?php if(isset($dt_jns_barang)){foreach ($dt_jns_barang as $row){?>
                                <option value="<?php echo $row->kd_jns_barang?>"><?php echo $row->jns_barang?></option>
                            <?php } }?>
                        </select>
                    </div>

                    <div id="optionBarang"></div>

                    <div id="dataBarang"></div>

                    <div class="form-group">
                        <label>Jumlah Permintaan</label>
                        <input id="qty" name="qty" class="form-control" type="text" required="">
                    </div>

                    <div class="modal-footer" style="display: none">
                        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-inverse" type="submit">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#selectJenis").change(function(){
        var kd_jns_barang = {kd_jns_barang:$("#selectJenis option:selected").val()};
        $.ajax({
            type: "POST",
            url : "<?php echo base_url('barang/get_barang_ajax'); ?>",
            data: kd_jns_barang,
            success: function(msg){
                $('#optionBarang').html(msg);
            }
        });
    });

    $(document).ready(function() {
        $(".delbutton").click(function(){
            var element = $(this);
            var del_id = element.attr("id");
            var info = del_id;
            if(confirm("Anda yakin akan menghapus data ini ?")){
                $.ajax({
                    url: "<?php echo base_url();?>permintaan/delete_data_cart",
                    data: "kode="+info,
                    cache: false,
                    success: function(){
                        window.location.reload(true);
                    }
                });
            }
            return false;
        });
    })
</script>