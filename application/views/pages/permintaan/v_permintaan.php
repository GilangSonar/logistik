<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-edit"></i>
                            <span>Permintaan</span>
                        </h1>
                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Transaksi
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Permintaan</li>
                            </ul>
                        </div>
                    </div>

                    <!--// END HEADER //-->

                    <div class="row">
                        <div class="col-sm-12">

                            <form action="<?php echo site_url('permintaan/delete_permintaan')?>" method="post">
                                <div class="box bordered-box fb-border" style="margin-bottom:0;">
                                    <div class='box-header'>
                                        <div class='title'>
                                            List Permintaan
                                        </div>
                                        <div class='actions'>
                                            <a class="btn btn-primary" href="<?php echo site_url('permintaan/add_page')?>">
                                                <i class="icon-plus-sign"></i> Tambah Data
                                            </a>
                                        </div>
                                    </div>

                                    <div class="box-content box-no-padding">
                                        <div class="responsive-table">

                                            <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">No</th>
                                                    <th>Kode</th>
                                                    <th>Permintaan</th>
                                                    <th>Dibutuhkan</th>
                                                    <th>Jumlah</th>
                                                    <th>Unit</th>
                                                    <th>Status</th>
                                                    <th class="text-center">
                                                        <i class="icon-th-large"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php $no=1; if ($this->session->userdata('LEVEL') == "admin" or $this->session->userdata('LEVEL') == "gudang") {
                                                        if (isset($dt_permintaan)){ foreach ($dt_permintaan as $row){ ?>

                                                    <tr class="<?php if($row->stts == 'ok'){echo "text-error";}?>">

                                                        <td class="text-center"><?php echo $no++;?></td>
                                                        <td><?php echo $row->kd_permintaan ?></td>
                                                        <td><?php echo date("d M Y",strtotime($row->tgl_permintaan));?></td>
                                                        <td><?php echo date("d M Y",strtotime($row->tgl_dibutuhkan));?></td>
                                                        <td><?php echo $row->jumlah?> Item</td>
                                                        <td><?php echo $row->nm_unit?></td>
                                                        <td><?php echo $row->stts?></td>
                                                        <td class="col-xs-2 text-center">
                                                            <div class="btn-group">
                                                                <a href="<?php echo site_url('permintaan/detail_permintaan/'.$row->kd_permintaan) ?>"
                                                                   class="btn has-tooltip btn-primary" data-placement="top" title="View Detail">
                                                                    <i class="icon-zoom-in"></i>
                                                                </a>
                                                                <a href="<?php echo site_url('permintaan/edit_page/'.$row->kd_permintaan) ?>"
                                                                   class="btn has-tooltip btn-primary <?php if($row->stts == 'ok'){echo "disabled";}?>" data-placement="top" title="Edit">
                                                                    <i class="icon-edit"></i>
                                                                </a>
                                                                <a href="<?php echo site_url('permintaan/delete_permintaan/'.$row->kd_permintaan) ?>"
                                                                   class="btn has-tooltip btn-primary" data-placement="top" title="Delete" onclick="return confirm('Anda Yakin Akan Hapus Data Ini ?');">
                                                                    <i class="icon-trash"></i>
                                                                </a>

                                                            </div>
                                                        </td>
                                                    </tr>

                                                <?php } }
                                                }else{
                                                    $no=1; if (isset($dt_permintaan_unit)){ foreach ($dt_permintaan_unit as $row){ ?>

                                                        <tr>
                                                            <td class="text-center"><?php echo $no++;?></td>
                                                            <td><?php echo $row->kd_permintaan ?></td>
                                                            <td><?php echo date("d M Y",strtotime($row->tgl_permintaan));?></td>
                                                            <td><?php echo date("d M Y",strtotime($row->tgl_dibutuhkan));?></td>
                                                            <td><?php echo $row->jumlah?> Item</td>
                                                            <td><?php echo $row->nm_unit?></td>
                                                            <td class="col-xs-2 text-center">
                                                                <div class="btn-group">

                                                                    <a href="<?php echo site_url('permintaan/detail_permintaan/'.$row->kd_permintaan) ?>"
                                                                       class="btn has-tooltip btn-primary" data-placement="top" title="View Detail">
                                                                        <i class="icon-zoom-in"></i>
                                                                    </a>
                                                                    <a href="#"
                                                                       class="btn has-tooltip btn-primary <?php if($row->stts == 'ok'){echo "disabled";}?>" data-placement="top" title="Edit">
                                                                        <i class="icon-edit"></i>
                                                                    </a>
                                                                    <a href="<?php echo site_url('permintaan/delete_permintaan/'.$row->kd_permintaan) ?>"
                                                                       class="btn has-tooltip btn-primary" data-placement="top" title="Delete" onclick="return confirm('Anda Yakin Akan Hapus Data Ini ?');">
                                                                        <i class="icon-trash"></i>
                                                                    </a>

                                                                </div>
                                                            </td>
                                                        </tr>

                                                <?php } }
                                                }?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                    <!-- // END CONTENT // -->
                    
                </div>
            </div>
        </div>
    </section>
</div>