<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="group-header">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="text-center">
                                    <i class="icon-3x icon-barcode center-block"></i>
                                    <h1>SISTEM LOGISTIK - TF HOTEL</h1>
                                    <h4 class="text-muted">
                                        Sistem Logistik ini dibuat untuk dapat membantu perusahaan dalam pengelolaan data permintaan barang, pengadaan barang & pengeluaran barang.
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </section>

</div>