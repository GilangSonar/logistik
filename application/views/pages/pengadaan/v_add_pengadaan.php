<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-edit"></i>
                            <span>Tambah Pengadaan</span>
                        </h1>
                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Transaksi
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('pengadaan')?>">
                                        <i class="icon-edit"></i> Pengadaan
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Tambah Pengadaan</li>
                            </ul>
                        </div>
                    </div>
                    <!-- // END HEADER // -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box bordered-box fb-border">
                                <div class="box-header fb-background">
                                    <div class="title">
                                        Form Pengadaan Barang
                                    </div>
                                </div>

                                <div class="box-content">
                                    <form class="form" method="post" action="<?php echo site_url('pengadaan/input_pengadaan')?>">
                                        <div class="box-toolbox box-toolbox-top">
                                            <div class="row">
                                                <br/>
                                                <div class="col-sm-6">
                                                    <p>
                                                        <strong>Kode Pengadaan Barang:</strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="kd_pengadaan"
                                                           value="<?php echo $kd_pengadaan?>" readonly>
                                                </div>

                                                <div class="col-sm-6">
                                                    <p>
                                                        <strong>Tanggal Pengadaan Barang: </strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="tgl_pengadaan"
                                                           value="<?php echo date('d M Y')?>" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-header">
                                            <div class="title">
                                                Isi Daftar Barang Masuk
                                            </div>
                                            <div class="actions">
                                                <a class="btn btn-primary" href="#modal-add" data-toggle="modal">
                                                    <i class="icon-reorder"></i> Pilih Barang
                                                </a>
                                            </div>
                                        </div>
                                        <div class="responsive-table">
                                            <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                                <thead>
                                                <tr>
                                                    <th>Kode</th>
                                                    <th>Nama Barang</th>
                                                    <th>Pengadaan</th>
                                                    <th>Harga</th>
                                                    <th>Subtotal</th>
                                                    <th class="text-center">
                                                        <i class="icon-th-large"></i>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <?php $i=1;?>
                                                <?php foreach($this->cart->contents() as $items): ?>
                                                    <?php echo form_hidden('rowid[]', $items['rowid']); ?>
                                                <tbody>
                                                <tr>
                                                    <td><?php echo $items['id']; ?></td>
                                                    <td><?php echo $items['name']; ?></td>
                                                    <td><?php echo $items['qty']; ?></td>
                                                    <td><?php echo currency_format($items['price']); ?></td>
                                                    <td><?php echo currency_format($items['subtotal']); ?></td>
                                                    <td class="col-xs-1">
                                                        <div class="text-center">
                                                            <a href="#" id="<?php echo 'hapus/'.$items['rowid'];?>" class="btn btn-danger btn-xs btn-block delbutton">
                                                                <i class="icon-trash"></i> Delete
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <?php $i++;?>
                                                <?php endforeach; ?>
                                            </table>
                                            <div class="box-content box-statistic text-left">
                                                <h3 class="title text-fb">Total Harga : <?php echo currency_format($this->cart->total()); ?> </u></h3>
                                            </div>
                                        </div>

                                        <div class="box-toolbox box-toolbox-bottom">
                                            <div class="row">
                                                <br/>
                                                <div class="col-sm-4">
                                                    <p>
                                                        <strong> Supplier Penyedia Barang : </strong>
                                                    </p>
                                                    <select class="form-control" id="select2" name="kd_supplier" required="">
                                                        <option value=""></option>
                                                        <?php if(isset($dt_supplier)){foreach ($dt_supplier as $row){?>
                                                            <option value="<?php echo $row->kd_supplier?>"><?php echo $row->nm_supplier?></option>
                                                        <?php } }?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>

                                        <input type="hidden" value="<?php echo $this->cart->total(); ?>" name="total_harga"/>
                                        <input type="hidden" value="<?php $this->session->userdata('ID')?>" name="kd_member"/>

                                        <hr class="hr-normal"/>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="icon-save"></i> Save
                                                </button>
                                                <a href="<?php echo site_url('pengadaan')?>" class="btn btn-default">
                                                    <i class="icon-remove-circle"></i> Cancel</a>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- // END CONTENT // -->

        </div>
    </section>
</div>

<!--MODAL ADD DATA-->
<div class="modal" id="modal-add" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">Pilih Barang</h4>
            </div>
            <div class="modal-body">

                <form class="form" style="margin-bottom: 0;" method="post" action="<?php echo site_url('pengadaan/add_barang_masuk')?>">
                    <div class="form-group">
                        <label>Jenis Barang</label>
                        <select class="form-control" id="selectJenis" name="kd_jns_barang" required="">
                            <option value=""></option>
                            <?php if(isset($dt_jns_barang)){foreach ($dt_jns_barang as $row){?>
                                <option value="<?php echo $row->kd_jns_barang?>"><?php echo $row->jns_barang?></option>
                            <?php } }?>
                        </select>
                    </div>

                    <div id="optionBarang"></div>

                    <div id="dataBarang"></div>

                    <div class="form-group">
                        <label>Jumlah Barang Masuk</label>
                        <input name="qty" class="form-control" type="text" required="">
                    </div>

                    <div class="modal-footer" style="display: none">
                        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-inverse" type="submit">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#selectJenis").change(function(){
        var kd_jns_barang = {kd_jns_barang:$("#selectJenis option:selected").val()};
        $.ajax({
            type: "POST",
            url : "<?php echo base_url('barang/get_barang_ajax'); ?>",
            data: kd_jns_barang,
            success: function(msg){
                $('#optionBarang').html(msg);
            }
        });
    });

    $(document).ready(function() {
        $(".delbutton").click(function(){
            var element = $(this);
            var del_id = element.attr("id");
            var info = del_id;
            if(confirm("Anda yakin akan menghapus data ini ?")){
                $.ajax({
                    url: "<?php echo base_url();?>pengadaan/delete_data_cart",
                    data: "kode="+info,
                    cache: false,
                    success: function(){
                        window.location.reload(true);
                    }
                });
            }
            return false;
        });
    })
</script>