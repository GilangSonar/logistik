<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id="content">
        <div class="container">

            <!-- // CONTENT // -->

            <div id="laporanPage">

                <div class="row" id="content-wrapper">
                    <div class="col-xs-12">
                        <div class="page-header page-header-with-buttons">
                            <h1 class="pull-left">
                                <i class="icon-file-text"></i>
                                <span>Laporan Pengadaan</span>
                            </h1>
                            <div class="pull-right">
                                <ul class="breadcrumb">
                                    <li>
                                        <a href="<?php echo site_url('dahsboard')?>">
                                            <i class="icon-dashboard"></i> Dashboard
                                        </a>
                                    </li>
                                    <li class="separator">
                                        <i class="icon-angle-right"></i>
                                    </li>
                                    <li>
                                        Laporan
                                    </li>
                                    <li class="separator">
                                        <i class="icon-angle-right"></i>
                                    </li>
                                    <li class="active">Laporan Pengadaan</li>
                                </ul>
                            </div>
                        </div>

                        <!-- // END HEADER // -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="box bordered-box fb-border">
                                    <div class="box-header fb-background">
                                        <div class="title">
                                            Laporan Pengadaan Per Tanggal
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <form class="form validate-form" method="post" action="<?php echo site_url('lap_pengadaan/cari')?>">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Dari Tanggal : </label>
                                                        <div class="datepicker-input input-group" id="datepicker">
                                                            <input class="form-control" name="tgl_awal" placeholder="Pilih Tanggal Awal" type="text" required="">
                                                            <span class="input-group-addon">
                                                                <span data-date-icon="icon-calendar" data-time-icon="icon-time"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Sampai Tanggal : </label>
                                                        <div class="datepicker-input input-group" id="datepicker">
                                                            <input class="form-control" name="tgl_akhir" placeholder="Pilih Tanggal Akhir" type="text" required="">
                                                            <span class="input-group-addon">
                                                                <span data-date-icon="icon-calendar" data-time-icon="icon-time"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <hr class="hr-normal"/>

                                            <div class="row">
                                                <div class="col-md-12 form-inline">
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="icon-search"></i> Pencarian Data ...
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // END CONTENT // -->

                    </div>
                </div>

            </div>
        </div>
    </section>
</div>


