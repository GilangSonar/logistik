<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-cogs hidden-print"></i>
                            <span>Daftar Persediaan Barang</span>
                        </h1>
                        <h3 class="visible-print">
                            <span><?php echo date('d M Y')?></span>
                        </h3>
                        <div class="pull-right hidden-print">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Master Data
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Barang</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                            <form action="<?php echo site_url('barang/delete_barang')?>" method="post">
                                <div class="box bordered-box fb-border" style="margin-bottom:0;">
                                    <div class='box-header hidden-print'>
                                        <div class='title'>
                                            List Barang
                                        </div>
                                        <div class='actions'>
                                            <a class="btn btn-primary" href="#modal-add" data-toggle="modal" role="button">
                                                <i class="icon-plus-sign"></i> Tambah Data
                                            </a>
                                            <button class="btn btn-primary" type="submit" onclick="return confirm('Anda yankin akan menghapus data ini ?');">
                                                <i class="icon-trash"></i> Delete Checked Data
                                            </button>
                                        </div>
                                    </div>

                                    <div class="box-content box-no-padding">
                                        <div class="responsive-table">

                                            <!-- ==================== // HIDDEN ON PRINT // ==============-->
                                            <div class="hidden-print">
                                                <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                                    <thead>
                                                    <tr>
                                                        <th class="only-checkbox text-center">
                                                            <input class="check-all" type="checkbox">
                                                        </th>
                                                        <th>No</th>
                                                        <th>Kode</th>
                                                        <th>Jenis</th>
                                                        <th>Nama</th>
                                                        <th>Harga</th>
                                                        <th>Stok</th>
                                                        <th class="text-center">
                                                            <i class="icon-th-large"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_barang)){foreach ($dt_barang as $row){ ?>
                                                        <tr>
                                                            <td class="only-checkbox text-center">
                                                                <input type="checkbox" name="cek[]" value="<?php echo $row->kd_barang;?>">
                                                            </td>
                                                            <td><?php echo $no++ ?></td>
                                                            <td><?php echo $row->kd_barang ?></td>
                                                            <td><?php echo $row->jns_barang ?></td>
                                                            <td><?php echo $row->nm_barang ?></td>
                                                            <td>
                                                                <div class="text-right"><?php echo currency_format($row->harga_barang)?></div>
                                                            </td>
                                                            <td>
                                                                <div class="text-center"><?php echo $row->stok ?></div>
                                                            </td>
                                                            <td class="col-xs-1">
                                                                <div class="text-center">
                                                                    <a class="btn btn-inverse btn-xs" href="#modal-edit<?php echo $row->kd_barang?>" data-toggle="modal" role="btn">
                                                                        <i class="icon-edit"></i> Edit
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } } ?>

                                                    </tbody>
                                                </table>
                                            </div>

                                            <!-- ==================== // VISIBLE ON PRINT // ==============-->

                                            <div class="visible-print">
                                                <table class="table" style="margin-bottom:0;">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode</th>
                                                        <th>Jenis</th>
                                                        <th>Nama</th>
                                                        <th>
                                                            <div class="text-right">Harga</div>
                                                        </th>
                                                        <th>
                                                            <div class="text-center">Stok</div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_barang)){foreach ($dt_barang as $row){ ?>
                                                        <tr>
                                                            <td><?php echo $no++ ?></td>
                                                            <td><?php echo $row->kd_barang ?></td>
                                                            <td><?php echo $row->jns_barang ?></td>
                                                            <td><?php echo $row->nm_barang ?></td>
                                                            <td>
                                                                <div class="text-right"><?php echo currency_format($row->harga_barang)?></div>
                                                            </td>
                                                            <td>
                                                                <div class="text-center"><?php echo $row->stok ?></div>
                                                            </td>
                                                        </tr>
                                                    <?php } } ?>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="form-actions hidden-print">
                                <a class="btn btn-default" onclick="window.print()">
                                    <i class="icon-print"></i> Print Data
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<!--MODAL ADD DATA-->
<div class="modal fade hidden-print" id="modal-add" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
            </div>
            <div class="modal-body">

                <form class="form" style="margin-bottom: 0;" method="post" action="<?php echo site_url('barang/input_barang')?>">
                    <div class="form-group">
                        <label>Kode Barang</label>
                        <input name="kd_barang" class="form-control" type="text" value="<?php echo $kd_barang?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Jenis Barang</label>
                        <select class="form-control" name="kd_jns_barang">
                            <?php if(isset($dt_jns_barang)){foreach($dt_jns_barang as $row){?>
                            <option value="<?php echo $row->kd_jns_barang?>"><?php echo $row->jns_barang?></option>
                            <?php } } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input name="nm_barang" class="form-control" type="text" required="">
                    </div>
                    <div class="form-group">
                        <label>Harga Barang</label>
                        <input name="harga_barang" class="form-control" type="text" required="">
                    </div>

                    <div class="form-group">
                        <label>Stok Barang</label>
                        <input name="stok" class="form-control" type="text" required="">
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-inverse" type="submit">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<!--MODAL EDIT DATA-->

<?php if(isset($dt_barang)){foreach ($dt_barang as $row){ ?>

    <div class="modal fade" id="modal-edit<?php echo $row->kd_barang?>" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
                </div>
                <div class="modal-body">

                    <form class="form" style="margin-bottom: 0;" method="post" action="<?php echo site_url('barang/edit_barang')?>">
                        <div class="form-group">
                            <label>Kode Barang</label>
                            <input name="kd_barang" class="form-control" type="text" value="<?php echo $row->kd_barang?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Jenis Barang</label>
                            <select class="form-control" name="kd_jns_barang">
                                <?php if(isset($dt_jns_barang)){foreach($dt_jns_barang as $row2){?>
                                    <option value="<?php echo $row2->kd_jns_barang?>"><?php echo $row2->jns_barang?></option>
                                <?php } } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input name="nm_barang" class="form-control" type="text" value="<?php echo $row->nm_barang?>" required="">
                        </div>
                        <div class="form-group">
                            <label>Harga Barang</label>
                            <input name="harga_barang" class="form-control" type="text" value="<?php echo $row->harga_barang?>" required="">
                        </div>

                        <div class="form-group">
                            <label>Stok Barang</label>
                            <input name="stok" class="form-control" type="text" value="<?php echo $row->stok?>" required="">
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                            <button class="btn btn-inverse" type="submit">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

<?php } } ?>