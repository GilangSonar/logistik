<?php if(isset($barang_by_id)){foreach ($barang_by_id as $row){ ?>

    <input name="nm_barang" type="hidden" value="<?php echo $row->nm_barang?>">
    <input name="harga_barang" type="hidden" value="<?php echo $row->harga_barang?>">

    <div class="form-group">
        <label>Stok Barang</label>
        <input id="stok" name="stok" class="form-control" type="text" value="<?php echo $row->stok?>" readonly>
    </div>

<?php } } ?>

<script type="text/javascript">

    //    RULES VALIDATION PERMINTAAN
    $(document).ready(function(){
        $(".val-form-permintaan").validate({
            rules: {
                qty: {
                    required:true,
                    max: $('#stok').val()
                }
            }
        });
    })
</script>