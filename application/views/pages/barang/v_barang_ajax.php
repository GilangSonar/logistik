<div class="form-group">
    <label>Daftar Barang</label>
    <select class="form-control" id="selectBarang" name="kd_barang" required="">
        <option value=""></option>
        <?php if(isset($ajax_brg)){foreach ($ajax_brg as $row){?>
            <option value="<?php echo $row->kd_barang?>"><?php echo $row->nm_barang?></option>
        <?php } }?>
    </select>
</div>

<script>
    $("select").select2({
        placeholder: " = Pilihan = ",
        allowClear: true
    });

    $("#selectBarang").change(function(){
        var kd_barang = {kd_barang:$("#selectBarang option:selected").val()};
        $.ajax({
            type: "POST",
            url : "<?php echo base_url('barang/get_data_barang'); ?>",
            data: kd_barang,
            success: function(msg){
                $('#dataBarang').html(msg);
                $('.modal-footer').show();
            }
        });
    });
</script>