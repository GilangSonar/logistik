<script type="text/javascript">
    $(function(){
        $("#btnCari").click(function() {
            var $form = $('#laporanPage').find('form'),
                $tgl_awal = $("#tgl_awal").val(),
                $tgl_akhir = $("#tgl_akhir").val(),
                $url = $form.attr('action');
            $.ajax({
                type: "POST",
                url: $url,
                dataType: "html",
                data: "tgl_awal="+$tgl_awal+"&tgl_akhir="+$tgl_akhir,
                cache:false,
                success: function(data){
                    $(".loader").fadeIn(500).fadeOut(500).queue(function(){
                        $('#laporanPage').html(data);
                    });
                }
            });
            return false;
        });
    });
</script>
<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id="content">
        <div class="container">

            <!-- // CONTENT // -->
            <div class="row" id="content-wrapper">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <h1 class="pull-left">
                                    <i class="icon-file-text  hidden-print"></i>
                                    <span>Laporan Pengeluaran</span>
                                </h1>
                                <h3 class="visible-print">
                                    <span><?php echo $tgl_awal?> - <?php echo $tgl_akhir?></span>
                                </h3>
                                <div class="pull-right  hidden-print">
                                    <div class="btn-group">

                                        <a class="btn btn-primary" href="<?php echo site_url("lap_pengeluaran")?>">
                                            <i class="icon-rotate-left"></i> Kembali
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ==================== // HIDDEN ON PRINT // ==============-->
                    <div class="row invoice hidden-print">
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-content box-double-padding">
                                    <div class="row">
                                        <div class="invoice-header">
                                            <div class="invoice-title">
                                                Per Tanggal
                                                <small class="text-muted"><?php echo $tgl_awal?> - <?php echo $tgl_akhir?></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="responsive-table">
                                                <div class="scrollable-area">
                                                    <table class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Kode Pengeluaran</th>
                                                            <th>Kode Permintaan</th>
                                                            <th>Tgl. Pengeluaran</th>
                                                            <th>Unit Peminta</th>
                                                            <th>Jumlah Barang</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <?php $no=1; if(isset($dt_lap_pengeluaran)){foreach ($dt_lap_pengeluaran as $row){?>
                                                            <tr>
                                                                <td><?php echo $no++; ?></td>
                                                                <td><?php echo $row->kd_pengeluaran; ?></td>
                                                                <td><?php echo $row->kd_permintaan; ?></td>
                                                                <td><?php echo date("d M Y",strtotime($row->tgl_pengeluaran));?></td>
                                                                <td><?php echo $row->nm_unit; ?></td>
                                                                <td><?php echo $row->jumlah; ?> Items</td>
                                                            </tr>
                                                        <?php } } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions hidden-print">
                                <a class="btn btn-default" onclick="window.print()">
                                    <i class="icon-print"></i> Print Data
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- ==================== // VISIBLE ON PRINT // ==============-->
                    <div class="visible-print">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="box">
                                    <div class="box-content box-no-padding">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode Pengeluaran</th>
                                                        <th>Kode Permintaan</th>
                                                        <th>Tgl. Pengeluaran</th>
                                                        <th>Unit Peminta</th>
                                                        <th>Jumlah Barang</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_lap_pengeluaran)){foreach ($dt_lap_pengeluaran as $row){?>
                                                        <tr>
                                                            <td><?php echo $no++; ?></td>
                                                            <td><?php echo $row->kd_pengeluaran; ?></td>
                                                            <td><?php echo $row->kd_permintaan; ?></td>
                                                            <td><?php echo date("d M Y",strtotime($row->tgl_pengeluaran));?></td>
                                                            <td><?php echo $row->nm_unit; ?></td>
                                                            <td><?php echo $row->jumlah; ?> Items</td>
                                                        </tr>
                                                    <?php } } ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ==================== // END VISIBLE ON PRINT // ==============-->

                </div>
            </div>


        </div>
    </section>
</div>