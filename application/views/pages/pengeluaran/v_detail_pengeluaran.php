<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class="row" id="content-wrapper">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <h1 class="pull-left">
                                    <i class="icon-file-text  hidden-print"></i>
                                    <span>Detail Pengeluaran</span>
                                </h1>

                                <div class="pull-right  hidden-print">
                                    <div class="btn-group">
                                        <a class="btn btn-primary" onclick="window.history.go(-1)">
                                            <i class="icon-rotate-left"></i> Kembali
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ==================== // HIDDEN ON PRINT // ==============-->
                    <div class="row invoice hidden-print">
                        <div class="col-sm-12">
                            <div class="box">
                                <div class="box-content box-double-padding">

                                    <?php if(isset($dt_pengeluaran_header)){foreach ($dt_pengeluaran_header as $row ){ ?>
                                        <div class="row">
                                            <div class="col-sm-4 seller">
                                                <address>
                                                    <div class="lead text-contrast"># <?php echo $row->kd_pengeluaran?></div>
                                                    <strong>Tgl Pengeluaran: </strong> <?php echo date('d/M/Y',strtotime($row->tgl_pengeluaran))?>
                                                    <br>
                                                </address>
                                            </div>
                                            <div class="col-sm-4 seller">
                                                <address>
                                                    <div class="lead text-contrast"># <?php echo $row->kd_permintaan?></div>
                                                    <strong>Tgl Permintaan: </strong> <?php echo date('d/M/Y',strtotime($row->tgl_permintaan))?>
                                                    <br>
                                                    <strong>Tgl Dibutuhkan: </strong> <?php echo date('d/M/Y',strtotime($row->tgl_dibutuhkan))?>
                                                    <br>
                                                    <strong>Unit Peminta:</strong> <?php echo $row->nm_unit?>
                                                    <br>
                                                </address>
                                            </div>
                                            <div class="col-sm-4 seller">
                                                <address>
                                                    <strong>Total Harga :</strong><div class="lead text-contrast"><?php echo currency_format($row->total_harga)?></div>
                                                </address>
                                            </div>
                                        </div>
                                    <?php } } ?>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="responsive-table">
                                                <div class="scrollable-area">
                                                    <table class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Kode Barang</th>
                                                            <th>Nama Barang</th>
                                                            <th>Qty</th>
                                                            <th>Harga Barang</th>
                                                            <th>Subtotal</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <?php $no=1; if(isset($dt_pengeluaran_detail)){foreach ($dt_pengeluaran_detail as $row ){ ?>
                                                            <tr>
                                                                <td><?php echo $no++; ?></td>
                                                                <td><?php echo $row->kd_barang; ?></td>
                                                                <td><?php echo $row->nm_barang; ?></td>
                                                                <td><?php echo $row->qty; ?></td>
                                                                <td><?php echo currency_format($row->harga_barang); ?></td>
                                                                <td><?php echo currency_format($row->qty*$row->harga_barang);?></td>
                                                            </tr>
                                                        <?php } } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions hidden-print">
                                <a class="btn btn-default" onclick="window.print()">
                                    <i class="icon-print"></i> Print Data
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- ==================== // VISIBLE ON PRINT // ==============-->
                    <div class="visible-print">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php if(isset($dt_pengeluaran_header)){foreach ($dt_pengeluaran_header as $row ){ ?>
                                    <div class="row">
                                        <div class="col-sm-4 seller">
                                            <address>
                                                <div class="lead text-contrast"># <?php echo $row->kd_pengeluaran?></div>
                                                <strong>Tgl Pengeluaran: </strong> <?php echo date('d/M/Y',strtotime($row->tgl_pengeluaran))?>
                                                <br>
                                            </address>
                                        </div>
                                        <div class="col-sm-4 seller">
                                            <address>
                                                <div class="lead text-contrast"># <?php echo $row->kd_permintaan?></div>
                                                <strong>Tgl Permintaan: </strong> <?php echo date('d/M/Y',strtotime($row->tgl_permintaan))?>
                                                <br>
                                                <strong>Tgl Dibutuhkan: </strong> <?php echo date('d/M/Y',strtotime($row->tgl_dibutuhkan))?>
                                                <br>
                                                <strong>Unit Peminta:</strong> <?php echo $row->nm_unit?>
                                                <br>
                                            </address>
                                        </div>
                                        <div class="col-sm-4 seller">
                                            <address>
                                                <strong>Total Harga :</strong><div class="lead text-contrast"><?php echo currency_format($row->total_harga)?></div>
                                            </address>
                                        </div>
                                    </div>
                                <?php } } ?>

                                <div class="box">
                                    <div class="box-content box-no-padding">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode Barang</th>
                                                        <th>Nama Barang</th>
                                                        <th>Qty</th>
                                                        <th>Harga Barang</th>
                                                        <th>Subtotal</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $no=1; if(isset($dt_pengeluaran_detail)){foreach ($dt_pengeluaran_detail as $row ){ ?>
                                                        <tr>
                                                            <td><?php echo $no++; ?></td>
                                                            <td><?php echo $row->kd_barang; ?></td>
                                                            <td><?php echo $row->nm_barang; ?></td>
                                                            <td><?php echo $row->qty; ?></td>
                                                            <td><?php echo currency_format($row->harga_barang); ?></td>
                                                            <td><?php echo currency_format($row->qty*$row->harga_barang);?></td>
                                                        </tr>
                                                    <?php } } ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ==================== // END VISIBLE ON PRINT // ==============-->

                </div>
            </div>
        </div>
    </section>
</div>
