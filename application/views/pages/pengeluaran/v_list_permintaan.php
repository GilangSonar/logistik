<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-edit"></i>
                            <span>Tambah Pengeluaran</span>
                        </h1>

                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Transaksi
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('pengeluaran')?>">
                                        <i class="icon-edit"></i> Pengeluaran
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Tambah Pengeluaran</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box bordered-box fb-border" style="margin-bottom:0;">
                                <div class="box-header">
                                    <div class="title">
                                        Daftar Permintaan Pending
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-primary" href="<?php echo site_url('pengeluaran')?>">
                                            <i class="icon-rotate-left"></i> Kembali
                                        </a>
                                    </div>
                                </div>

                                <div class="box-content box-no-padding">
                                    <div class="responsive-table">

                                        <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                            <thead>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Permintaan</th>
                                                <th>Dibutuhkan</th>
                                                <th>Jumlah</th>
                                                <th>Unit</th>
                                                <th class="text-center">
                                                    <i class="icon-th-large"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php if (isset($dt_permintaan_pending)){ foreach ($dt_permintaan_pending as $row){ ?>
                                                <tr>
                                                    <td><?php echo $row->kd_permintaan ?></td>
                                                    <td><?php echo date("d M Y",strtotime($row->tgl_permintaan));?></td>
                                                    <td><?php echo date("d M Y",strtotime($row->tgl_dibutuhkan));?></td>
                                                    <td><?php echo $row->jumlah?> Item</td>
                                                    <td><?php echo $row->nm_unit?></td>
                                                    <td class="col-xs-1">
                                                        <div class="text-center">
                                                            <a href="<?php echo site_url('pengeluaran/add_page/'.$row->kd_permintaan)?>"
                                                               class="btn has-tooltip btn-primary" data-placement="top" title="Apply Permintaan">
                                                                <i class="icon-check-sign"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } }?>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- // END CONTENT // -->

                </div>
            </div>
        </div>
    </section>

</div>