<!-- // TOPBAR // -->
<?php $this->load->view('subelement/v_top')?>

<div id='wrapper'>

    <!-- // SIDEBAR // -->
    <?php $this->load->view('subelement/v_sidebar')?>
    <section id='content'>
        <div class='container'>

            <!-- // CONTENT // -->
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>

                    <div class="page-header page-header-with-buttons">
                        <h1 class="pull-left">
                            <i class="icon-edit"></i>
                            <span>Tambah Pengeluaran</span>
                        </h1>
                        <div class="pull-right">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo site_url('dahsboard')?>">
                                        <i class="icon-dashboard"></i> Dashboard
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Transaksi
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('pengeluaran')?>">
                                        <i class="icon-edit"></i> Pengeluaran
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Tambah Pengeluaran</li>
                            </ul>
                        </div>
                    </div>
                    <!-- // END HEADER // -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box bordered-box fb-border">
                                <div class="box-header fb-background">
                                    <div class="title">
                                        Form Pengeluaran Barang
                                    </div>
                                </div>

                                <div class="box-content">
                                    <form class="form" method="post" action="<?php echo site_url('pengeluaran/input_pengeluaran')?>">
                                        <div class="box-toolbox box-toolbox-top">

                                            <?php if(isset($dt_permintaan_header_id)){foreach($dt_permintaan_header_id as $row){ ?>
                                            <div class="row">
                                                <br/>
                                                <div class="col-sm-3">
                                                    <p>
                                                        <strong>Kode Pengeluaran Barang:</strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="kd_pengeluaran"
                                                           value="<?php echo $kd_pengeluaran?>" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <p>
                                                        <strong>Tanggal Pengeluaran Barang: </strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="tgl_pengeluaran"
                                                           value="<?php echo date('d M Y')?>" readonly>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <br/>
                                                <div class="col-sm-3">
                                                    <p>
                                                        <strong> Kode Permintaan: </strong>
                                                    </p>
                                                    <input class="form-control" type="text" name="kd_permintaan"
                                                           value="<?php echo $row->kd_permintaan?>" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <p>
                                                        <strong> Unit Peminta Barang : </strong>
                                                    </p>
                                                    <input class="form-control" type="text" value="<?php echo $row->nm_unit?>" readonly>
                                                    <input type="hidden" name="kd_unit" value="<?php echo $row->kd_unit?>">
                                                </div>
                                                <div class="col-sm-3">
                                                    <p>
                                                        <strong>Tanggal Permintaan Barang: </strong>
                                                    </p>
                                                    <input class="form-control" type="text"
                                                           value="<?php echo date("d M Y",strtotime($row->tgl_permintaan));?>" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <p>
                                                        <strong>Tanggal Dibutuhkan: </strong>
                                                    </p>
                                                    <input class="form-control" type="text"
                                                           value="<?php echo date("d M Y",strtotime($row->tgl_dibutuhkan));?>" readonly>
                                                </div>
                                            </div>

                                            <?php } } ?>
                                        </div>

                                        <div class="box-header">
                                            <div class="title">
                                                Isi Daftar Barang Keluar
                                            </div>
                                        </div>
                                        <div class="responsive-table">
                                            <table class="data-table table table-bordered table-striped table-hover" style="margin-bottom:0;">
                                                <thead>
                                                <tr>
                                                    <th>Kode</th>
                                                    <th>Nama Barang</th>
                                                    <th>Permintaan</th>
                                                    <th>Harga</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                                </thead>
                                                <?php $i=1;?>
                                                <?php foreach($this->cart->contents() as $items): ?>
                                                    <?php echo form_hidden('rowid[]', $items['rowid']); ?>
                                                <tbody>
                                                <tr>
                                                    <td><?php echo $items['id']; ?></td>
                                                    <td><?php echo $items['name']; ?></td>
                                                    <td><?php echo $items['qty']; ?></td>
                                                    <td><?php echo currency_format($items['price']); ?></td>
                                                    <td><?php echo currency_format($items['subtotal']); ?></td>
                                                </tr>
                                                </tbody>
                                                <?php $i++;?>
                                                <?php endforeach; ?>
                                            </table>
                                            <div class="box-content box-statistic text-left">
                                                <h3 class="title text-fb">Total Harga : <?php echo currency_format($this->cart->total()); ?> </u></h3>
                                            </div>
                                        </div>

                                        <input type="hidden" value="<?php echo $this->cart->total(); ?>" name="total_harga"/>

                                        <div class="form-actions form-actions-padding-sm">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="icon-save"></i> Save
                                                    </button>
                                                    <a href="<?php echo site_url('pengeluaran')?>" class="btn btn-default">
                                                        <i class="icon-remove-circle"></i> Cancel</a>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- // END CONTENT // -->

        </div>
    </section>
</div>