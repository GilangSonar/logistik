<!-- // Jquery Validate // -->
<script src="<?php echo base_url('asset/js/plugins/validate/jquery.validate.min.js')?>" type="text/javascript"></script>
<!-- // Data Table // -->
<script src="<?php echo base_url('asset/js/plugins/datatables/jquery.dataTables.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('asset/js/plugins/datatables/dataTables.overrides.js')?>" type="text/javascript"></script>
<!-- // Jquery-Gritter // -->
<script src="<?php echo base_url('asset/js/plugins/jquery_gritter/jquery.gritter.min.js')?>" type="text/javascript"></script>
<!-- // Select 2 // -->
<script src="<?php echo base_url('asset/js/plugins/select2/select2.js')?>" type="text/javascript"></script>
<!-- // Bootstrap Datetime Picker // -->
<script src="<?php echo base_url('asset/js/plugins/common/moment.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('asset/js/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.js')?>" type="text/javascript"></script>

<script type="text/javascript">
    $("select").select2({
        placeholder: " = Pilihan = ",
        allowClear: true
    });
</script>


</body>
</html>