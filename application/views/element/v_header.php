<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title;?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="text/html;charset=utf-8" http-equiv="content-type">
    <meta content="Logistic System - TF Hotel" name="description">

    <!-- ICONS -->
    <link href="<?php echo base_url('asset/img/ico/favicon.png')?>" rel="shortcut icon" type="image/x-icon">
    <link href="<?php echo base_url('asset/img/ico/apple-touch-icon-57-precomposed')?>" rel="apple-touch-icon-precomposed" sizes="57x57">
    <link href="<?php echo base_url('asset/img/ico/apple-touch-icon-72-precomposed')?>" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="<?php echo base_url('asset/img/ico/apple-touch-icon-114-precomposed')?>" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="<?php echo base_url('asset/img/ico/apple-touch-icon-144-precomposed')?>" rel="apple-touch-icon-precomposed" sizes="144x144">

    <!-- PLUGINS CSS -->
    <link href="<?php echo base_url('asset/css/plugins/datatables/bootstrap-datatable.css')?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/plugins/jquery_gritter/jquery.gritter.css')?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/plugins/select2/select2.css')?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css')?>" media="all" rel="stylesheet" type="text/css" />

    <!-- BASE CSS -->
    <link href="<?php echo base_url('asset/css/bootstrap.css')?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/dark-theme.css')?>" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/theme-colors.css')?>" media="all" rel="stylesheet" type="text/css" />

    <!--[if lt IE 9]>
    <script src="assets/javascripts/ie/html5shiv.js" type="text/javascript"></script>
    <script src="assets/javascripts/ie/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <!-- BASE JS-->
    <script src="<?php echo base_url('asset/js/jquery/jquery.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/jquery/jquery.mobile.custom.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/jquery/jquery-migrate.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/jquery/jquery-ui.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/bootstrap/bootstrap.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/plugins/modernizr/modernizr.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/js/plugins/retina/retina.js')?>" type="text/javascript"></script>

    <!-- CUSTOM THEME JS -->
    <script src="<?php echo base_url('asset/js/theme.js')?>" type="text/javascript"></script>

</head>

<body class="contrast-fb fixed-header fixed-navigation">
