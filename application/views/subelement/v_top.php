<header>
<nav class="navbar navbar-default navbar-fixed-top">
<a class="navbar-brand" href="<?php echo site_url('dashboard')?>">
    <div class="logo">
        <i class="icon-barcode"></i>
        <strong>Logistic - TF Hotel</strong>
    </div>
    <div class="logo-xs">
        <i class="icon-barcode"></i>
    </div>

</a>
<a class="toggle-nav btn pull-left" href="#">
    <i class="icon-reorder"></i>
</a>
<ul class="nav">

    <li class="dropdown medium only-icon widget">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <?php if($notif_stok_barang->num_rows() > 0){
                $r= $notif_stok_barang->num_rows();
                if($r > 0 ){ ?>

                    <i class="icon-globe"></i> <strong><?php echo $r?></strong>

                <?php
                }
            }else{ ?>

                <i class="icon-globe"></i>

            <?php } ?>
        </a>
        <ul class="dropdown-menu">

            <?php if(isset($notif_stok_barang)){ foreach($notif_stok_barang->result() as $row){ ?>
            <li>
                <a href="<?php echo site_url('barang')?>">
                    <div class="widget-body">
                        <div class="pull-left icon">
                            <i class="icon-chevron-sign-right text-red"></i>
                        </div>
                        <div class="pull-left">
                            <?php echo $row->nm_barang?> - Stok Barang = <?php echo $row->stok?>
                        </div>
                    </div>
                </a>
            </li>

            <li class="divider"></li>
            <?php } } ?>
            <li class="widget-footer">
                <a href="<?php echo site_url('pengadaan')?>">
                    <strong>Peringatan! Sisa Stok barang di atas kurang dari 5. Harap segera lakukan proses pengadaan </strong>
                </a>
            </li>

        </ul>
    </li>
    <li class="dropdown dark user-menu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="user-name"><?php echo $this->session->userdata('NAME') ?></span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="<?php echo site_url('member/change_page/'.$this->session->userdata('ID'))?>">
                    <i class="icon-key"></i>
                    Change Your Password
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('panel/logout')?>">
                    <i class="icon-signout"></i>
                    Sign out
                </a>
            </li>
        </ul>
    </li>
</ul>
</nav>
</header>