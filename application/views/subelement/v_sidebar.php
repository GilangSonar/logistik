<div id="main-nav-bg"></div>
<nav class="main-nav-fixed" id="main-nav">
    <div class="navigation">
        <ul class="nav nav-stacked">

            <?php if ($this->session->userdata('LEVEL') == 'admin') { ?>

                <li class="<?php if(isset($act_dashboard)){echo $act_dashboard; }?>">
                    <a href="<?php echo site_url('dashboard')?>">
                        <i class="icon-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="<?php if(isset($act_transaksi)){echo $act_transaksi; }?>">
                    <a class="dropdown-collapse" href="#"><i class="icon-edit"></i>
                        <span>Transaksi</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>

                    <ul class="nav nav-stacked <?php if(isset($show_transaksi)){echo $show_transaksi; }?>">
                        <li class="<?php if(isset($act_permintaan)){echo $act_permintaan; }?>">
                            <a href="<?php echo site_url('permintaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Permintaan</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_pengeluaran)){echo $act_pengeluaran; }?>">
                            <a href="<?php echo site_url('pengeluaran')?>">
                                <i class="icon-caret-right"></i>
                                <span>Pengeluaran</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_pengadaan)){echo $act_pengadaan; }?>">
                            <a href="<?php echo site_url('pengadaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Pengadaan</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php if(isset($act_laporan)){echo $act_laporan; }?>">
                    <a class="dropdown-collapse " href="#">
                        <i class="icon-file-text"></i>
                        <span>Laporan</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>
                    <ul class="nav nav-stacked <?php if(isset($show_laporan)){echo $show_laporan; }?>">
                        <li class="<?php if(isset($act_lap_permintaan)){echo $act_lap_permintaan; }?>">
                            <a href="<?php echo site_url('lap_permintaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Laporan Permintaan</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_lap_pengeluaran)){echo $act_lap_pengeluaran; }?>">
                            <a href="<?php echo site_url('lap_pengeluaran')?>">
                                <i class="icon-caret-right"></i>
                                <span>Laporan Pengeluaran</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_lap_pengadaan)){echo $act_lap_pengadaan; }?>">
                            <a href="<?php echo site_url('lap_pengadaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Laporan Pengadaan</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php if(isset($act_master)){echo $act_master; }?>">
                    <a class="dropdown-collapse " href="#">
                        <i class="icon-cogs"></i>
                        <span>Master Data</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>
                    <ul class="nav nav-stacked <?php if(isset($show_master)){echo $show_master; }?>">
                        <li class="<?php if(isset($act_jns_brg)){echo $act_jns_brg; }?>">
                            <a href="<?php echo site_url('jenis_barang')?>">
                                <i class="icon-caret-right"></i>
                                <span>Jenis Barang</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_brg)){echo $act_brg; }?>">
                            <a href="<?php echo site_url('barang')?>">
                                <i class="icon-caret-right"></i>
                                <span>Barang</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_unit)){echo $act_unit; }?>">
                            <a href="<?php echo site_url('unit')?>">
                                <i class="icon-caret-right"></i>
                                <span>Unit / Bagian</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_supplier)){echo $act_supplier; }?>">
                            <a href="<?php echo site_url('supplier')?>">
                                <i class="icon-caret-right"></i>
                                <span>Supplier</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_member)){echo $act_member; }?>">
                            <a href="<?php echo site_url('member')?>">
                                <i class="icon-caret-right"></i>
                                <span>Member</span>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php }elseif($this->session->userdata('LEVEL') == 'gudang') {?>

                <li class="<?php if(isset($act_dashboard)){echo $act_dashboard; }?>">
                    <a href="<?php echo site_url('dashboard')?>">
                        <i class="icon-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="<?php if(isset($act_transaksi)){echo $act_transaksi; }?>">
                    <a class="dropdown-collapse" href="#"><i class="icon-edit"></i>
                        <span>Transaksi</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>

                    <ul class="nav nav-stacked <?php if(isset($show_transaksi)){echo $show_transaksi; }?>">
                        <li class="<?php if(isset($act_pengeluaran)){echo $act_pengeluaran; }?>">
                            <a href="<?php echo site_url('pengeluaran')?>">
                                <i class="icon-caret-right"></i>
                                <span>Pengeluaran</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_pengadaan)){echo $act_pengadaan; }?>">
                            <a href="<?php echo site_url('pengadaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Pengadaan</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="<?php if(isset($act_laporan)){echo $act_laporan; }?>">
                    <a class="dropdown-collapse " href="#">
                        <i class="icon-file-text"></i>
                        <span>Laporan</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>
                    <ul class="nav nav-stacked <?php if(isset($show_laporan)){echo $show_laporan; }?>">
                        <li class="<?php if(isset($act_lap_permintaan)){echo $act_lap_permintaan; }?>">
                            <a href="<?php echo site_url('lap_permintaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Laporan Permintaan</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_lap_pengeluaran)){echo $act_lap_pengeluaran; }?>">
                            <a href="<?php echo site_url('lap_pengeluaran')?>">
                                <i class="icon-caret-right"></i>
                                <span>Laporan Pengeluaran</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_lap_pengadaan)){echo $act_lap_pengadaan; }?>">
                            <a href="<?php echo site_url('lap_pengadaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Laporan Pengadaan</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?php if(isset($act_master)){echo $act_master; }?>">
                    <a class="dropdown-collapse " href="#">
                        <i class="icon-cogs"></i>
                        <span>Master Data</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>
                    <ul class="nav nav-stacked <?php if(isset($show_master)){echo $show_master; }?>">
                        <li class="<?php if(isset($act_jns_brg)){echo $act_jns_brg; }?>">
                            <a href="<?php echo site_url('jenis_barang')?>">
                                <i class="icon-caret-right"></i>
                                <span>Jenis Barang</span>
                            </a>
                        </li>
                        <li class="<?php if(isset($act_brg)){echo $act_brg; }?>">
                            <a href="<?php echo site_url('barang')?>">
                                <i class="icon-caret-right"></i>
                                <span>Barang</span>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php }else{ ?>

                <li class="<?php if(isset($act_dashboard)){echo $act_dashboard; }?>">
                    <a href="<?php echo site_url('dashboard')?>">
                        <i class="icon-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="<?php if(isset($act_transaksi)){echo $act_transaksi; }?>">
                    <a class="dropdown-collapse" href="#"><i class="icon-edit"></i>
                        <span>Transaksi</span>
                        <i class="icon-angle-down angle-down"></i>
                    </a>

                    <ul class="nav nav-stacked <?php if(isset($show_transaksi)){echo $show_transaksi; }?>">

                        <li class="<?php if(isset($act_permintaan)){echo $act_permintaan; }?>">
                            <a href="<?php echo site_url('permintaan')?>">
                                <i class="icon-caret-right"></i>
                                <span>Permintaan</span>
                            </a>
                        </li>

                    </ul>
                </li>

            <?php } ?>
        </ul>
    </div>
</nav>

