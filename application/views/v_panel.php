<html>
<head>
    <title><?php echo $title;?></title>

    <!-- ICONS -->
    <link href="<?php echo base_url('asset/img/ico/favicon.png')?>" rel="shortcut icon" type="image/x-icon">

    <!-- PLUGINS CSS-->
    <link href="<?php echo base_url('asset/css/plugins/jquery_gritter/jquery.gritter.css')?>" media="all" rel="stylesheet" type="text/css" />

    <!-- BASE CSS -->
    <link href="<?php echo base_url('asset/css/bootstrap.css')?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/dark-theme.css')?>" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('asset/css/theme-colors.css')?>" media="all" rel="stylesheet" type="text/css" />

    <!-- BASE JS-->
    <script src="<?php echo base_url('asset/js/jquery/jquery.min.js')?>" type="text/javascript"></script>

    <!-- // Jquery-Gritter // -->
    <script src="<?php echo base_url('asset/js/plugins/jquery_gritter/jquery.gritter.min.js')?>" type="text/javascript"></script>

    <!-- CUSTOM THEME JS -->
    <script type="text/javascript">
        $(function(){
            $("#btnLogin").click(function() {
                var $form = $('#loginform').find('form'),
                    $username = $("#username").val(),
                    $password = $("#password").val(),
                    $url = $form.attr('action');
                $.ajax({
                    type: "POST",
                    url: $url,
                    dataType: "text",
                    data: "username="+$username+"&password="+$password,
                    cache:false,
                    success: function(data){
                        $(".loader").fadeIn(500).fadeOut(500).queue(function(){
                            window.location = "<?php echo site_url('dashboard') ?>";
                        });
                    }
                });
                return false;
            });
        });

    </script>
</head>

<body class="contrast-fb login contrast-background">
<?php $this->load->view('subelement/v_notification'); ?>

<div class="middle-container">
    <div class="middle-row">
        <div class="middle-wrapper">
            <div class="login-container-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <i class="icon-2x icon-barcode"></i>
                                <h2 class="text-muted">SISTEM LOGISTIK - TF HOTEL</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container">
                <div class="container">
                    <div class="row">
                        <div id="loginform" class="col-sm-4 col-sm-offset-4">
                            <form action="<?php echo site_url('panel/login')?>" method="post">

                                <div class="text-center">
                                    <!-- NOTIF FAILED LOGIN-->
                                    <?php
                                    $message = $this->session->flashdata('notif');
                                    if($message){
                                        echo '<div class="text-fb">' . $message . '</div>';
                                    }?>

                                    <!-- LOADER IMAGE -->
                                    <img class="loader" src="<?php echo base_url('asset/img/loader.gif') ?>" alt="Loader" style="display: none"/>
                                </div>

                                <hr class="hr-normal"/>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input id="username" placeholder="Username" class="form-control" name="username" type="text"  required=""/>
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <input id="password" placeholder="Password" class="form-control" name="password" type="password" required=""/>
                                        <i class="icon-lock text-muted"></i>
                                    </div>
                                </div>
                                <hr class="hr-normal"/>
                                <button id="btnLogin" type="submit" class="btn btn-block btn-primary">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-container-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <a href="#">
                                    &copy; Copyright - Gilang Sonar & Andreas Hirasna
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
