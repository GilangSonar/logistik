<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Unit extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Unit',
            'act_unit'=>'active',
            'show_master'=>'in',
            'act_master'=>'active',

            'kd_unit'=>$this->m_master->kodeUnit(),
            'dt_unit'=>$this->m_master->getAllUnit(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/unit/v_unit');
        $this->load->view('element/v_footer');
    }
    function input_unit(){
        $data = array(
            'kd_unit'=>$this->input->post('kd_unit'),
            'nm_unit'=>$this->input->post('nm_unit'),
        );
        $this->m_master->insertUnit($data);
        $this->session->set_flashdata('success_add',true);
        redirect("unit");
    }
    function edit_unit(){
        $id=$this->input->post('kd_unit');
        $data = array(
            'nm_unit'=>$this->input->post('nm_unit'),
        );
        $this->m_master->updateUnit($id,$data);
        $this->session->set_flashdata('success_update',true);
        redirect("unit");
    }
    function delete_unit(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('unit');
        }else{
            $this->m_master->deleteUnit($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('unit');
    }

}