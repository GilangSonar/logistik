<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Pengadaan extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
        $this->load->model('m_transaksi');
        $this->load->helper('currency_format_helper');
    }

    function index(){
        $data=array(
            'title'=>'Pengadaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengadaan'=>'active',

            'dt_pengadaan'=>$this->m_transaksi->getPengadaan(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->session->unset_userdata('edit_cart');
        $this->cart->destroy();
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/pengadaan/v_pengadaan');
        $this->load->view('element/v_footer');
    }
    function add_page(){
        $data=array(
            'title'=>'Tambah Pengadaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengadaan'=>'active',

            'kd_pengadaan'=>$this->m_transaksi->kodePengadaan(),
            'dt_supplier'=>$this->m_master->getAllSupplier(),
            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/pengadaan/v_add_pengadaan');
        $this->load->view('element/v_footer');
    }
    function add_barang_masuk(){
        $data = array(
            'id'        => $this->input->post('kd_barang'),
            'qty'       => $this->input->post('qty'),
            'price'     => $this->input->post('harga_barang'),
            'name'      => $this->input->post('nm_barang'),
        );
        $this->cart->insert($data);
        redirect('pengadaan/add_page');
    }
    function input_pengadaan(){
        $data=array(
            'kd_pengadaan'=>$this->input->post('kd_pengadaan'),
            'kd_supplier'=>$this->input->post('kd_supplier'),
            'total_harga'=>$this->input->post('total_harga'),
            'kd_member'=>$this->session->userdata('ID'),
            'tgl_pengadaan'=>date("Y-m-d",strtotime($this->input->post('tgl_pengadaan'))),
        );
        $this->m_transaksi->insertPengadaanHeader($data);

        foreach($this->cart->contents() as $items){
            $dt_detail['kd_pengadaan'] = $this->input->post('kd_pengadaan');
            $dt_detail['kd_barang'] = $items['id'];
            $dt_detail['qty'] = $items['qty'];
            $this->m_transaksi->insertPengadaanDetail($dt_detail);

            $id['kd_barang'] = $dt_detail['kd_barang'];
            $dt['stok'] = $this->m_transaksi->tambahStok($dt_detail['kd_barang'],$dt_detail['qty']);
            $this->m_master->updateBarang($id['kd_barang'],$dt);
        }
        $this->cart->destroy();
        $this->session->set_flashdata('success_add',true);
        redirect('pengadaan');
    }

    function detail_pengadaan(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Detail Pengadaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengadaan'=>'active',

            'dt_pengadaan_header'=>$this->m_transaksi->getPengadaanHeaderById($id),
            'dt_pengadaan_detail'=>$this->m_transaksi->getPengadaanDetailById($id),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->cart->destroy();
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/pengadaan/v_detail_pengadaan');
        $this->load->view('element/v_footer');
    }
    function edit_page(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Edit Pengadaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengadaan'=>'active',

            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'dt_pengadaan_header_id'=>$this->m_transaksi->getPengadaanHeaderById($id),
            'dt_supplier'=>$this->m_master->getAllSupplier(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $dt['pengadaan_detail_id']=$this->m_transaksi->getPengadaanDetailById($id);
        if($this->session->userdata("edit_cart")==""){
            $in_cart=array();
            foreach($dt['pengadaan_detail_id'] as $row ){
                $in_cart[] = array(
                    'id'=>$row->kd_barang,
                    'qty'=>$row->qty,
                    'price'=>$row->harga_barang,
                    'name'=>$row->nm_barang,
                );
                $sess_data['kode_pengadaan'] = $row->kd_pengadaan;
            }
            $this->cart->insert($in_cart);
            $sess_data['edit_cart'] = "edit";
            $this->session->set_userdata($sess_data);
        }
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/pengadaan/v_edit_pengadaan',$dt);
        $this->load->view('element/v_footer');
    }
    function edit_barang_masuk(){
        $data = array(
            'id'    => $this->input->post('kd_barang'),
            'qty'   => $this->input->post('qty'),
            'price' => $this->input->post('harga_barang'),
            'name'  => $this->input->post('nm_barang'),
        );
        $this->cart->insert($data);
        redirect('pengadaan/edit_page/'.$this->session->userdata('kode_pengadaan'));
    }
    function update_pengadaan(){
        $id=$this->input->post('kd_pengadaan');
        $data=array(
            'kd_supplier'=>$this->input->post('kd_supplier'),
            'total_harga'=>$this->input->post('total_harga'),
            'kd_member'=>$this->session->userdata('ID'),
            'tgl_pengadaan'=>date("Y-m-d",strtotime($this->input->post('tgl_pengadaan'))),
        );
        $this->m_transaksi->updatePengadaanHeader($id,$data);

        $q = $this->m_transaksi->getPengadaanDetailById($id);
        foreach($q as $row){
            $key= $row->kd_barang;
            $data= array('stok'=>$this->m_transaksi->kembalikanStok($row->kd_barang) - $row->qty);
            $this->m_master->updateBarang($key,$data);
        }

        $this->m_transaksi->deletePengadaanDetail($id);
        foreach($this->cart->contents() as $items){
            $dt_detail['kd_pengadaan'] = $this->input->post('kd_pengadaan');
            $dt_detail['kd_barang'] = $items['id'];
            $dt_detail['qty'] = $items['qty'];
            $this->m_transaksi->insertPengadaanDetail($dt_detail);

            $kd['kd_barang'] = $dt_detail['kd_barang'];
            $dt['stok'] = $this->m_transaksi->tambahStok($dt_detail['kd_barang'],$dt_detail['qty']);
            $this->m_master->updateBarang($kd['kd_barang'],$dt);
        }
        $this->session->unset_userdata('kode_pengadaan');
        $this->session->unset_userdata('edit_cart');
        $this->cart->destroy();
        $this->session->set_flashdata('success_add',true);
        redirect('pengadaan');
    }

    function delete_data_cart(){
        $id = $this->uri->segment(3);
        $dt['dt_pengadaan_header'] = $this->m_transaksi->getPengadaanHeaderById($id);
        foreach($dt['dt_pengadaan_header'] as $row){
            $sess_data['kode_pengadaan'] = $row->kd_pengadaan;
            $this->session->set_userdata($sess_data);
        }
        $kode = explode("/",$_GET['kode']);
        if($kode[0]=="hapus"){
            $data = array(
                'rowid' => $kode[1],
                'qty'   => 0
            );
            $this->cart->update($data);
        }
        redirect('pengadaan/edit_page/'.$this->session->userdata('kode_pengadaan'));
    }

    function delete_pengadaan(){
        $id = $this->uri->segment(3);
        $q = $this->m_transaksi->getPengadaanDetailById($id);
        foreach($q as $row){
            $key= $row->kd_barang;
            $data= array('stok'=>$this->m_transaksi->kembalikanStok($row->kd_barang) - $row->qty);
            $this->m_master->updateBarang($key,$data);
        }
        $this->m_transaksi->deletePengadaan($id);
        $this->session->set_flashdata('success_delete',true);
        redirect('pengadaan');
    }
    
}