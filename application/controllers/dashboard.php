<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/17/13
 * Time: 10:50 AM
 */

class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Dashboard | Logistic System - TF Hotel',
            'act_dashboard'=>'active',

            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/dashboard/v_dashboard');
        $this->load->view('element/v_footer');
    }
}