<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Jenis_barang extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Jenis Barang',
            'act_jns_brg'=>'active',
            'show_master'=>'in',
            'act_master'=>'active',

            'kd_jns_barang'=>$this->m_master->kodeJenisBarang(),
            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/jenis_barang/v_jenis_barang');
        $this->load->view('element/v_footer');
    }

    function input_jenis_barang(){
        $data = array(
            'kd_jns_barang'=>$this->input->post('kd_jns_barang'),
            'jns_barang'=>$this->input->post('jns_barang'),
        );
        $this->m_master->insertJenisBarang($data);
        $this->session->set_flashdata('success_add',true);
        redirect("jenis_barang");
    }
    function edit_jenis_barang(){
        $id=$this->input->post('kd_jns_barang');
        $data = array(
            'jns_barang'=>$this->input->post('jns_barang'),
        );
        $this->m_master->updateJenisBarang($id,$data);
        $this->session->set_flashdata('success_update',true);
        redirect("jenis_barang");
    }
    function delete_jenis_barang(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('jenis_barang');
        }else{
            $this->m_master->deleteJenisBarang($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('jenis_barang');
    }
}