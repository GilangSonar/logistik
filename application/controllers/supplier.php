<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Supplier extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Supplier',
            'act_supplier'=>'active',
            'show_master'=>'in',
            'act_master'=>'active',

            'kd_supplier'=>$this->m_master->kodeSupplier(),
            'dt_supplier'=>$this->m_master->getAllSupplier(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/supplier/v_supplier');
        $this->load->view('element/v_footer');
    }
    function input_supplier(){
        $data = array(
            'kd_supplier'=>$this->input->post('kd_supplier'),
            'nm_supplier'=>$this->input->post('nm_supplier'),
            'alamat'=>$this->input->post('alamat'),
            'kontak'=>$this->input->post('kontak'),
            'email'=>$this->input->post('email'),
        );
        $this->m_master->insertSupplier($data);
        $this->session->set_flashdata('success_add',true);
        redirect("supplier");
    }
    function edit_supplier(){
        $id=$this->input->post('kd_supplier');
        $data = array(
            'nm_supplier'=>$this->input->post('nm_supplier'),
            'alamat'=>$this->input->post('alamat'),
            'kontak'=>$this->input->post('kontak'),
            'email'=>$this->input->post('email'),
        );
        $this->m_master->updateSupplier($id,$data);
        $this->session->set_flashdata('success_update',true);
        redirect("supplier");
    }
    function delete_supplier(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('supplier');
        }else{
            $this->m_master->deleteSupplier($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('supplier');
    }
}