<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Pengeluaran extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
        $this->load->model('m_transaksi');
        $this->load->helper('currency_format_helper');
    }

    function index(){
        $data=array(
            'title'=>'Pengeluaran',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengeluaran'=>'active',

            'dt_pengeluaran'=>$this->m_transaksi->getPengeluaran(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->cart->destroy();
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/pengeluaran/v_pengeluaran');
        $this->load->view('element/v_footer');
    }
    function list_permintaan(){
        $data=array(
            'title'=>'List Permintaan Pending',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengeluaran'=>'active',

            'dt_permintaan_pending'=>$this->m_transaksi->getPermintaanPending(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/pengeluaran/v_list_permintaan');
        $this->load->view('element/v_footer');
    }
    function add_page(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Tambah Pengeluaran',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengeluaran'=>'active',

            'kd_pengeluaran'=>$this->m_transaksi->kodePengeluaran(),
            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'dt_permintaan_header_id'=>$this->m_transaksi->getPermintaanHeaderById($id),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $dt['permintaan_detail_id']=$this->m_transaksi->getPermintaanDetailById($id);
        $in_cart=array();
        foreach($dt['permintaan_detail_id'] as $row ){
            $in_cart[] = array(
                'id'=>$row->kd_barang,
                'qty'=>$row->qty,
                'price'=>$row->harga_barang,
                'name'=>$row->nm_barang,
            );
        }
        $this->cart->insert($in_cart);
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/pengeluaran/v_add_pengeluaran',$dt);
        $this->load->view('element/v_footer');
    }
    function input_pengeluaran(){
        $data=array(
            'kd_pengeluaran'=>$this->input->post('kd_pengeluaran'),
            'kd_permintaan'=>$this->input->post('kd_permintaan'),
            'kd_unit'=>$this->input->post('kd_unit'),
            'total_harga'=>$this->input->post('total_harga'),
            'kd_member'=>$this->session->userdata('ID'),
            'tgl_pengeluaran'=>date("Y-m-d",strtotime($this->input->post('tgl_pengeluaran'))),
        );
        $this->m_transaksi->insertPengeluaranHeader($data);

        $kd = $this->input->post('kd_permintaan');
        $stts=array('stts'=>'ok');
        $this->m_transaksi->updatePermintaanHeader($kd,$stts);

        foreach($this->cart->contents() as $items){
            $dt_detail['kd_pengeluaran'] = $this->input->post('kd_pengeluaran');
            $dt_detail['kd_barang'] = $items['id'];
            $dt_detail['qty'] = $items['qty'];
            $this->m_transaksi->insertPengeluaranDetail($dt_detail);

            $id['kd_barang'] = $dt_detail['kd_barang'];
            $dt['stok'] = $this->m_transaksi->kurangiStok($dt_detail['kd_barang'],$dt_detail['qty']);
            $this->m_master->updateBarang($id['kd_barang'],$dt);
        }
        $this->cart->destroy();
        $this->session->set_flashdata('success_add',true);
        redirect('pengeluaran');
    }
    function detail_pengeluaran(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Detail Pengeluaran',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_pengeluaran'=>'active',

            'dt_pengeluaran_header'=>$this->m_transaksi->getPengeluaranHeaderById($id),
            'dt_pengeluaran_detail'=>$this->m_transaksi->getPengeluaranDetailById($id),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->cart->destroy();
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/pengeluaran/v_detail_pengeluaran');
        $this->load->view('element/v_footer');
    }
    function delete_pengeluaran(){
        $id = $this->uri->segment(3);
        $q = $this->m_transaksi->getPengeluaranDetailById($id);
        foreach($q as $row){
            $key= $row->kd_barang;
            $data= array('stok'=>$this->m_transaksi->kembalikanStok($row->kd_barang) + $row->qty);
            $this->m_master->updateBarang($key,$data);
        }
        $q2 = $this->m_transaksi->getPengeluaranHeaderById($id);
        foreach($q2 as $row){
            $key= $row->kd_permintaan;
            $data= array('stts'=>'pending');
            $this->m_transaksi->updatePermintaanHeader($key,$data);
        }
        $this->m_transaksi->deletePengeluaran($id);
        $this->session->set_flashdata('success_delete',true);
        redirect('pengeluaran');
    }

}