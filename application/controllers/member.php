<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Member extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Member',
            'act_member'=>'active',
            'show_master'=>'in',
            'act_master'=>'active',

            'kd_member'=>$this->m_master->kodeMember(),
            'dt_member'=>$this->m_master->getAllMember(),
            'dt_unit'=>$this->m_master->getAllUnit(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/member/v_member');
        $this->load->view('element/v_footer');
    }
    function input_member(){
        $data = array(
            'kd_member'=>$this->input->post('kd_member'),
            'kd_unit'=>$this->input->post('kd_unit'),
            'nm_member'=>$this->input->post('nm_member'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'level'=>$this->input->post('level'),
        );
        $this->m_master->insertMember($data);
        $this->session->set_flashdata('success_add',true);
        redirect("member");
    }
    function edit_member(){
        $id=$this->input->post('kd_member');
        $data = array(
            'kd_unit'=>$this->input->post('kd_unit'),
            'nm_member'=>$this->input->post('nm_member'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'level'=>$this->input->post('level'),
        );
        $this->m_master->updateMember($id,$data);
        $this->session->set_flashdata('success_update',true);
        redirect("member");
    }
    function delete_member(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('member');
        }else{
            $this->m_master->deleteMember($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('member');
    }

    function change_page(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Change Password',
            'dt_member_id'=>$this->m_master->getMemberById($id),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/member/v_change_pass');
        $this->load->view('element/v_footer');

    }
    function change_password(){
        $id = $this->uri->segment(3);
        foreach($this->m_master->getMemberById($id) as $row){
            if($row->password == md5($this->input->post('oldpass'))){
                $data = array(
                    'username'=>$this->input->post('username'),
                    'password' => md5($this->input->post('password'))
                );
                $this->m_master->updateMember($id,$data);
                $this->session->unset_userdata('ID');
                $this->session->unset_userdata('NAME');
                $this->session->unset_userdata('USERNAME');
                $this->session->unset_userdata('PASS');
                $this->session->unset_userdata('LEVEL');
                $this->session->unset_userdata('UNIT');
                $this->session->set_flashdata('success_change_pass',true);
                redirect('');
            }else{
                $this->session->set_flashdata('failed_change_pass',true);
                redirect('member/change_page/'.$id);
            }
        }
    }
}