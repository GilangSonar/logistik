<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Lap_permintaan extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_laporan');
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Laporan Permintaan',
            'show_laporan'=>'in',
            'act_laporan'=>'active',
            'act_lap_permintaan'=>'active',
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->session->unset_userdata('tgl_awal');
        $this->session->unset_userdata('tgl_akhir');

        $this->load->view('element/v_header',$data);
        $this->load->view('pages/lap_permintaan/v_lap_permintaan');
        $this->load->view('element/v_footer');
    }

    function cari(){
        $tgl_awal= date("Y-m-d",strtotime($this->input->post('tgl_awal')));
        $tgl_akhir= date("Y-m-d",strtotime($this->input->post('tgl_akhir')));
        $sess_data=array(
            'tgl_awal'=>$tgl_awal,
            'tgl_akhir'=>$tgl_akhir
        );
        $this->session->set_userdata($sess_data);
        $data=array(
            'title'=>'Laporan Permintaan Per Tanggal',
            'show_laporan'=>'in',
            'act_laporan'=>'active',
            'act_lap_permintaan'=>'active',
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),

            'dt_lap_permintaan'=> $this->m_laporan->getLapPermintaan($tgl_awal,$tgl_akhir),
            'tgl_awal'=>date("d M Y",strtotime($this->session->userdata('tgl_awal'))),
            'tgl_akhir'=>date("d M Y",strtotime($this->session->userdata('tgl_akhir'))),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/lap_permintaan/v_hasil_lap_permintaan');
        $this->load->view('element/v_footer');
    }
}