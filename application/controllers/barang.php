<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Barang extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };

        $this->load->model('m_master');
        $this->load->helper('currency_format_helper');
    }

    function index(){
        $data=array(
            'title'=>'Barang',
            'act_brg'=>'active',
            'show_master'=>'in',
            'act_master'=>'active',

            'kd_barang'=>$this->m_master->kodeBarang(),
            'dt_barang'=>$this->m_master->getAllBarang(),
            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/barang/v_barang');
        $this->load->view('element/v_footer');
    }
    function get_barang_ajax(){
        $id=$this->input->post('kd_jns_barang');
        $data=array(
            'ajax_brg'=>$this->m_master->getBarangPengeluaran($id),
        );
        $this->load->view('pages/barang/v_barang_ajax',$data);
    }
    function get_data_barang(){
        $id=$this->input->post('kd_barang');
        $data=array(
            'barang_by_id'=>$this->m_master->getBarangById($id),
        );
        $this->load->view('pages/barang/v_data_barang',$data);
    }
    function input_barang(){
        $data = array(
            'kd_barang'=>$this->input->post('kd_barang'),
            'kd_jns_barang'=>$this->input->post('kd_jns_barang'),
            'nm_barang'=>$this->input->post('nm_barang'),
            'harga_barang'=>$this->input->post('harga_barang'),
            'stok'=>$this->input->post('stok'),
        );
        $this->m_master->insertBarang($data);
        $this->session->set_flashdata('success_add',true);
        redirect("barang");
    }
    function edit_barang(){
        $id=$this->input->post('kd_barang');
        $data = array(
            'kd_jns_barang'=>$this->input->post('kd_jns_barang'),
            'nm_barang'=>$this->input->post('nm_barang'),
            'harga_barang'=>$this->input->post('harga_barang'),
            'stok'=>$this->input->post('stok'),
        );
        $this->m_master->updateBarang($id,$data);
        $this->session->set_flashdata('success_update',true);
        redirect("barang");
    }
    function delete_barang(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('barang');
        }else{
            $this->m_master->deleteBarang($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('barang');
    }

}