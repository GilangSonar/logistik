<?php
class Panel extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('m_master');
    }

    function index(){
        $data=array(
            'title'=>'Login Page'
        );
        $this->load->view('v_panel',$data);
    }

    function login() {
        $username = $this->input->post('username');
        $pass = md5($this->input->post('password'));
        foreach ($this->m_master->getAllMember() as $r):
            if($username == " " || $pass == " "){
                $status = 0;
            }
            elseif ($username != $r->username || $pass != $r->password){
                $status = 0;
            }
            else{
                $status = 1;
                $data = array(
                    'ID'=> $r->kd_member,
                    'NAME' => $r->nm_member,
                    'USERNAME' => $r->username,
                    'PASS' => $r->password,
                    'LEVEL' => $r->level,
                    'UNIT'=> $r->kd_unit,
                );
                $this->session->set_userdata($data);
            }
            $output = '{ "status": "'.$status.'" }';
            echo $output;
        endforeach;
    }

    function logout() {
        $this->session->unset_userdata('ID');
        $this->session->unset_userdata('NAME');
        $this->session->unset_userdata('USERNAME');
        $this->session->unset_userdata('PASS');
        $this->session->unset_userdata('LEVEL');
        $this->session->unset_userdata('UNIT');

        redirect('');
    }
}
