<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/19/13
 * Time: 7:45 PM
 */

class Permintaan extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASS') != TRUE){
            $this->session->set_flashdata('notif','GAGAL LOGIN, PASTIKAN USERNAME & PASSWORD ANDA BENAR !');
            redirect('');
        };
        $this->load->model('m_master');
        $this->load->model('m_transaksi');
    }
    function index(){
        $id=$this->session->userdata('UNIT');
        $data=array(
            'title'=>'Permintaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_permintaan'=>'active',

            'dt_permintaan'=>$this->m_transaksi->getPermintaan(),
            'dt_permintaan_unit'=>$this->m_transaksi->getPermintaanByUnit($id),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->cart->destroy();
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/permintaan/v_permintaan');
        $this->load->view('element/v_footer');
    }

    function add_page(){
        $data=array(
            'title'=>'Buat Permintaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_permintaan'=>'active',

            'kd_permintaan'=>$this->m_transaksi->kodePermintaan(),
            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/permintaan/v_add_permintaan');
        $this->load->view('element/v_footer');
    }
    function add_barang_pesan(){
        $data = array(
            'id'        => $this->input->post('kd_barang'),
            'qty'       => $this->input->post('qty'),
            'price'     => $this->input->post('harga_barang'),
            'name'      => $this->input->post('nm_barang'),
        );
        $this->cart->insert($data);
        redirect('permintaan/add_page');
    }
    function input_permintaan(){
        $data=array(
            'kd_permintaan'=>$this->input->post('kd_permintaan'),
            'kd_unit'=>$this->input->post('kd_unit'),
            'stts'=>'pending',
            'tgl_permintaan'=>date("Y-m-d",strtotime($this->input->post('tgl_permintaan'))),
            'tgl_dibutuhkan'=>date("Y-m-d",strtotime($this->input->post('tgl_dibutuhkan'))),
            'kd_member'=>$this->session->userdata('ID'),
        );
        $this->m_transaksi->insertPermintaanHeader($data);

        foreach($this->cart->contents() as $items){
            $dt_detail['kd_permintaan'] = $this->input->post('kd_permintaan');
            $dt_detail['kd_barang'] = $items['id'];
            $dt_detail['qty'] = $items['qty'];
            $this->m_transaksi->insertPermintaanDetail($dt_detail);
        }
        $this->cart->destroy();
        $this->session->set_flashdata('success_add',true);
        redirect('permintaan');
    }

    function detail_permintaan(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Detail Permintaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_permintaan'=>'active',

            'dt_permintaan_header'=>$this->m_transaksi->getPermintaanHeaderById($id),
            'dt_permintaan_detail'=>$this->m_transaksi->getPermintaanDetailById($id),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $this->cart->destroy();
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/permintaan/v_detail_permintaan');
        $this->load->view('element/v_footer');
    }
    function edit_page(){
        $id=$this->uri->segment(3);
        $data=array(
            'title'=>'Edit Permintaan',
            'show_transaksi'=>'in',
            'act_transaksi'=>'active',
            'act_permintaan'=>'active',

            'dt_jns_barang'=>$this->m_master->getAllJenisBarang(),
            'dt_permintaan_header_id'=>$this->m_transaksi->getPermintaanHeaderById($id),
            'dt_unit'=>$this->m_master->getAllUnit(),
            'notif_stok_barang'=>$this->m_master->getNotifStokBarang(),
        );
        $dt['permintaan_detail_id']=$this->m_transaksi->getPermintaanDetailById($id);
        if($this->session->userdata("edit_cart")==""){
            $in_cart=array();
            foreach($dt['permintaan_detail_id'] as $row ){
                $in_cart[] = array(
                    'id'=>$row->kd_barang,
                    'qty'=>$row->qty,
                    'price'=>$row->harga_barang,
                    'name'=>$row->nm_barang,
                );
                $sess_data['kode_permintaan'] = $row->kd_permintaan;
            }
            $this->cart->insert($in_cart);
            $sess_data['edit_cart'] = "edit";
            $this->session->set_userdata($sess_data);
        }
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/permintaan/v_edit_permintaan',$dt);
        $this->load->view('element/v_footer');
    }
    function edit_barang_pesan(){
        $data = array(
            'id'    => $this->input->post('kd_barang'),
            'qty'   => $this->input->post('qty'),
            'price' => $this->input->post('harga_barang'),
            'name'  => $this->input->post('nm_barang'),
        );
        $this->cart->insert($data);
        redirect('permintaan/edit_page/'.$this->session->userdata('kode_permintaan'));
    }
    function update_permintaan(){
        $id=$this->input->post('kd_permintaan');
        $data=array(
            'kd_unit'=>$this->input->post('kd_unit'),
            'stts'=>'pending',
            'kd_member'=>$this->session->userdata('ID'),
            'tgl_permintaan'=>date("Y-m-d",strtotime($this->input->post('tgl_permintaan'))),
            'tgl_dibutuhkan'=>date("Y-m-d",strtotime($this->input->post('tgl_dibutuhkan'))),
        );
        $this->m_transaksi->updatePermintaanHeader($id,$data);

        $this->m_transaksi->deletePermintaanDetail($id);
        foreach($this->cart->contents() as $items){
            $dt_detail['kd_permintaan'] = $this->input->post('kd_permintaan');
            $dt_detail['kd_barang'] = $items['id'];
            $dt_detail['qty'] = $items['qty'];
            $this->m_transaksi->insertPermintaanDetail($dt_detail);
        }
        $this->session->unset_userdata('kode_permintaan');
        $this->session->unset_userdata('edit_cart');
        $this->cart->destroy();
        $this->session->set_flashdata('success_add',true);
        redirect('permintaan');
    }

    function delete_data_cart(){
        $id = $this->uri->segment(3);
        $dt['dt_permintaan_header'] = $this->m_transaksi->getPermintaanHeaderById($id);
        foreach($dt['dt_permintaan_header'] as $row){
            $sess_data['kode_permintaan'] = $row->kd_permintaan;
            $this->session->set_userdata($sess_data);
        }
        $kode = explode("/",$_GET['kode']);
        if($kode[0]=="hapus"){
            $data = array(
                'rowid' => $kode[1],
                'qty'   => 0
            );
            $this->cart->update($data);
        }
        redirect('permintaan/edit_page/'.$this->session->userdata('kode_permintaan'));
    }    
    function delete_permintaan(){
        $id = $this->uri->segment(3);
        $this->m_transaksi->deletePermintaan($id);
        $this->session->set_flashdata('success_delete',true);
        redirect('permintaan');
    }
}